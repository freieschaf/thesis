FILE ?=thesis

all:
	pdflatex '\nonstopmode\input $(FILE)'
	bibtex $(FILE)
	pdflatex '\batchmode\input {$(FILE)}'
	pdflatex '\batchmode\input $(FILE)'
	#rm -f *.toc *.aux *.log *.dvi *.blg *.bbl *~
#	dvipdf -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true $(FILE).dvi

clean:
	rm -f *.toc *.aux *.log *.dvi *.blg *.bbl *~

