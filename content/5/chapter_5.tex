At the onset of this thesis work, our main research aim was to investigate whether 
contemporary word embedding models could be adapted to obtain more fine-grained
representations of word meaning, as described in research question Q1 formulated in 
chapter \ref{chapter:intro}, by means of transitioning from representing word forms
%TODO re-read this and try to tune it, possibly after providing a better formulation for Q1
to representing word senses that can provide accurate representations of a lexicographic
inventory of word senses in a particular language.
%by means of learning representations for word senses
%that could be used to improve performance in NLP applications that deal with semantics. 
A more detailed plan to achieve this with the help of lexicographic resources was
drawn in research question Q2, and an inquiry into potential benefits of word sense 
representations towards automatizing expansion and maintenance of said lexicographic
resources was proposed through research question Q3.

In the rest of this chapter, we review the outcomes of this undertaking by examining
our work through the lens of the aforementioned research questions and the contributions
that resulted from this work. We also sketch possible future lines of research that
could be based on these developments.

\section{Conclusions}

% Q1

In addressing the research lines laid by question Q1,
we showed that it is indeed possible to adapt current neural word embedding models to
automatically represent the different meanings of a word in separate vectors in the 
article presented in chapter \ref{chapter:a1}. Additionally, this was achieved with
little computational overhead with respect to the original word-based model.
In the evaluation of the model described
in that article we learned that, given the number of senses expected from a word, the
model is able to separate and represent meanings associated with it by inspecting
instances in different contexts. While these meanings do not always correlate with
lexicographic definitions of a word's senses, this shows that the model learns meanings
associated with different usages of the word, which we posit as a device that might 
help list potentially new senses.

Furthermore, by implementing additional modifications to word embedding models in
the articles contained in chapters \ref{chapter:a2} and \ref{chapter:a3}, we showed
that not only is it possible for these models to learn to separate several meanings
of a word, but also to learn them solely from lexicographic data or a combination of
that with textual data from a corpus.

% Q2

Those efforts relate to research question Q2;
we demonstrated that lexicographic data can be leveraged by
these models to derive high quality word sense representations in a vector space.
In chapter \ref{chapter:a2} we showed a possible way of operationalizing this kind
of manually crafted knowledge to derive representations as real-valued vectors which
are useful in downstream applications such as WSD. In chapter \ref{chapter:a3} we
took advantage of that fact and used lexicographic data as a source of training data
in order to address shortcomings of a corpus-based model such as the one presented in
chapter \ref{chapter:a1}, resulting in word sense representations that better resemble
their lexicographic definitions. In this third model, enabling control over the 
influence of the two separate sources of data in the training process allowed us to
observe in detail the effects of lexicographic data in such a model, showing that 
it is possible to achieve a balance between lexicon and corpus as sources of data
in order to improve performance in downstream applications. 

% Q3

In the different evaluation strategies applied to these models, we demonstrated that
word sense embeddings 
%have a place as semantic features in NLP tasks by 
%improving performance over word embeddings. 
are conducive to improved semantic representations. Accurate representations of
individual word senses originate in semantically coherent vector spaces, as evidenced
by lists of nearest neighbors to distinct senses of polysemic words.
Furthermore, we observed an improvement in performance of word sense embeddings over
word embeddings at predicting semantic frame membership.
We also showed that the applications of these
semantic representations is not limited to established NLP problems, but are also
apt to help improve existing lexicographic resources by providing manipulable 
representations for lexicon entries that can be used to automate lexicographic
work. These results address our final research question, Q3.

Indeed, in the article contained in chapter \ref{chapter:a4} we showed that by means
of establishing links between automatically learned word sense embeddings and word 
senses listed in a lexicon, it is possible to generate suggestions of potentially
new entries for the lexicon extracted from a corpus along with linguistic evidence.
We propose that such a system be used to partly automate the task of expanding a
lexicon in a way that reduces the human work load. Another example of automating
the expansion of resources is described in chapter \ref{chapter:a5}: in this case,
word sense representations are successfully applied to the task of linking ambiguous
entries in an older thesaurus with word senses listed in a modern lexicon as a way of increasing
the accessibility of the thesaurus for use in NLP applications.

% Summary of contributions

In summary, throughout this thesis work we have demonstrated different approaches to 
automatically learning word sense representations from corpora and lexica in three
separate models, and demonstrated their relevance both in several NLP downstream applications
as well as in two instances of lexicographic resource improvement.

\section{Future work}

We envision several lines of research as a continuation of this thesis work.
On one hand, we acknowledge the possibility of improving the quality of word sense
embeddings as a way of delivering greater semantic representational power for NLP
applications; improving models used to learn the representations and providing
efficient evaluation methods are two key pieces in achieving increased quality in
word sense representations. 
On the other hand, we propose that there are additional applications in the 
context of lexicographic resource expansion on which word sense representations
could have an role.

Having shown that integrating lexicographic knowledge as part of the training data
does have a positive impact in the meaning representations learned by word sense
embedding models, we hypothesize that refining the mechanisms which are used to
extract that knowledge from resources might still yield further benefits regarding
the quality of representations. We explored several approaches to injecting 
data from a lexicon into the embedding model in chapter \ref{chapter:a3} and observed
the influence of this step into the resulting embeddings' performance on downstream
tasks. Continuing this line of inquiry in a more exhaustive study could help determine
an optimal mechanism for merging lexicographic information and corpora as training
data for models with similar architectures. 

A common topic of discussion when addressing the automatic generation of word sense
representations is the approach used to parameterize the number of senses per word.
In our models, we have chosen to follow the lead given by a lexicon to determine this
number, but this just relegates the question to lexicographers and we should note that
there is little agreement on the ideal sense granularity between different lexica. 
(See the discussion about this point in chapter \ref{chapter:resources}.) 
There have been attempts to automatize this parameterization based on contextual 
evidence from a corpus \citep{neelakantan2014efficient,kaageback2015neural,bartunov2016breaking} but, again,
the result depends heavily on how the underlying resource (in this case, the corpus) 
is constructed. In this case, the completeness of the sense inventory would depend
on the coverage of contexts in the corpus; i.e., if a sense is underrepresented in
the corpus, it would probably not acquire representation in the model.
From our point of view, this information would ideally be rooted in both lexicographic
input and distributional data. 
As such, we believe that a truly complete sense inventory
should not ignore lexicographic definitions of word senses, but those should be complemented
with senses emerging from language use as can be found in corpora; such information 
could effectively cover the gaps that might exist in the lexica.
Research dealing with modeling language change across time in corpora 
\citep{mitra2014s,hamilton2016diachronic,tahmasebi2017finding}
does provide interesting techniques that could be useful for this purpose. 
The empirical study of semantic change across time calls 
for a flexible definition of word meaning in order to allow for new word senses
to emerge and be captured in the semantic space, and thus provides examples of 
how to approach the construction of variable word sense inventories.
Combining sense discovery techniques from word sense induction 
\citep{pantel2002discovering,brody2009bayesian,amrami2018word} with 
lexicographic word sense definitions in order to define the sense inventory used by a 
representational model could help learning more reliable word sense representations 
with regards to achieving an adequate coverage based on the needs of the different
scenarios in which they are applied.

%TODO interpretability 
Deep neural networks, as data-driven models that learn to generalize 
are tightly related to representation learning and their application in NLP reaches
back to the early 2000s \citep{gers2001lstm,bengio2003neural}.
Recent developments in deep learning methods have enabled innovative approaches to what
kind of data can be used to train semantic representations, from strings of characters
rather than whole words \citep{bojanowski2017enriching,athiwaratkun2018probabilistic}
to complex features extracted from neural language models like internal representations of the
textual context \citep{peters2018deep,amrami2018word,devlin2018bert}. 
Such approaches avoid some constraints characteristic of models centered
around word forms as the main unit of linguistic information, which has the potential 
to evade problems related to conflation of multiple meanings 
by allowing more flexibility in choosing contexts used to learn the different meanings
of a word.

\pagebreak

Reiterating our discussion about evaluating representations in chapter \ref{chapter:evaluation},
it should be stated that aiming for {\em higher quality representations} without 
suitable tools to evaluate their quality would turn out to be a mindless endeavor.   
Particularly in the case of evaluating word sense representations, we have found that
the lack of sense-annotated resources and standard non-English benchmarks hinders 
the evaluation process and, as a result, it is an impediment to the development of
models. Based on that, we stress that creation and maintenance of resources is a key
piece in the struggle for better semantic representations.
It would also be important to achieve a standardized evaluation approach that settles
a principled evaluation strategy in terms of tasks, downstream applications, metrics,
and precisely defined semantic aspects to be tested. Counting with a broadly accepted evaluation
approach would allow for a streamlined process for new semantic representations to be
assessed relative to existing models. Some recent research efforts have been put forward 
towards achieving such an unified evaluation strategy for semantic representations
\citep{camacho2016find,raganato2017word}, but the community still needs to find a 
standard that is flexible enough to cater to the broad variety of approaches to
learning semantic representations.

Finally, we illustrated the possibilities of word sense representations applied to
resource expansion in chapters \ref{chapter:a3}, \ref{chapter:a4}, and \ref{chapter:a5}.
These serve as examples of how to alleviate a usual bottleneck in resource improvement:
time-consuming and expensive human labor. We are confident that there are many more
similar tasks that could benefit from semantic representations as a tool to expedite
such tasks, as a way to tackle the issues raised in the previous paragraph.


