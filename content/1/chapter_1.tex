
\section{Motivation}

% **********************************************************************************
% Remember there is no need to go into great detail about any model/paradigm/resource
% here, since each of those should be found in detail in their own chapters.
% If any idea about detailing something comes up here, think about moving it to the
% corresponding chapter.
% **********************************************************************************


During the last decade, computer assistance performed through the use
of human language is solidifying from a long-anticipated concept into an everyday 
sideshow that lets us interact with our ever-increasing layer of technological apparatus.
This inconspicuous success is owed to a sustained research effort in the different
fields that coexist under the umbrella of Language Technology (LT): Artificial Intelligence
(AI) applied to human language, computerized linguistic models, and speech technology.
% ML and NN

Interestingly, the comparatively fast development on LT that is occurring in the last 
few years, 
contextualized in the enthusiasm for any and all AI technologies that
appears to be the norm nowadays, % ? 
follows a long dry period known as {\em AI Winter}, starting in the late 1980s,
which decelerated progress in the field of AI motivated by lack of interest and, hence,
funding: ``At its low point, some computer scientists and software engineers avoided 
the term artificial intelligence for fear of being viewed as wild-eyed dreamers.''
\citep{markoff2005behind}.
That lack of interest was itself due to a number of reasons from failure to live up
to the hype created to budget-cutting policies for universities. Not small among these
factors was the unavailability of computational power needed for neural network
models to fulfill their potential. And part of today's more optimist standpoint is
precisely due to hardware advancements which increase the capabilities of neural networks.

However, the decade of the 1990s was not devoid of advances in LT. It was precisely 
during this period that the ``statistical revolution'' \citep{johnson2009statistical} 
took place, a paradigm switch from rule-based to data-driven systems: an increase in
available digital data and computational power favored informing systems with statistical
data over sets of rules grounded in linguistic theory.
In this context, meaning representation models based on statistics thrived. A family of
models focused on providing words with semantic representations in a vector space, 
usually configured using co-occurrence statistics gathered from corpora, grew and,
slowly but surely, started paving the way towards widespread adoption
\citep{deerwester1990indexing,schutze1993word,lund1996producing,landauer1997solution}.

% The success of word embeddings as meaning representations. Brief history, some examples
% and their shortcomings: meaning conflation.
%While models for obtaining vector representations of words and other lexical units 
%have been successfully developed from a variety of approaches during the past several
%decades (see, for example, Latent Semantic Analysis, LSA, \citealp{deerwester1990indexing}),
From a variety of approaches to obtain vector representations of words and other lexical
units,
representations learned by neural networks stemming from neural language
models \citep{bengio2003neural} have recently attracted the community's attention for their
efficiency generating accurate semantic representations from large collections of
text. Having high quality semantic representations has proven beneficial in a large
number of Natural Language Processing (NLP) tasks such as syntactic parsing 
\citep{socher2013parsing}, named entity recognition and chunking \citep{turian2010word},
part-of-speech tagging and semantic role labeling \citep{collobert2011natural}, or
sentiment analysis \citep{glorot2011domain}. This good record, paired with ML
advancements facilitated by increased accessibility to new and old, revisited powerful
neural network models, has resulted in a myriad of refined representation models.
%usually focused on word forms (or lemmas).
Given that the main data source on which these models feed is text, it is not 
surprising that the majority of these models focus on representing the key building
brick of that kind of data: word forms.

% Word sense embeddings: why? List some examples.
However, word representations suffer from a well-known limitation: they ignore
polysemy, homonymy, and other related phenomena by which one word form may have more
than one meaning. Word representation models, by forcing each word to be represented
by one vector, may conflate several meanings into one representation, making recovery
of an individual meaning difficult or impossible to achieve. 
Since in many cases these vectors are used to represent the input to NLP systems
that carry out the tasks on which they are applied, this misrepresentation is 
propagated through them early on and is hardly recoverable. This is the main issue
addressed in this thesis: to develop semantic representation models that are aware
of the multiple meanings of a word and consequently learn representations for each
of them.

% Resources: a thing of the old NLP or a valuable source of information? Also probably
% better with examples
To tackle this task, we build on previous work on recent word representation models
that learn automatically from text. However, as mentioned above, unannotated textual
data may not be the most adequate source of information from which to derive knowledge
about the different meanings, or senses, of a word, and producing annotations for
the large amounts of text that such models consume is usually unfeasible or unreliable. For this
reason we propose to engage an extra source of information where this missing 
knowledge is readily available: linguistic resources such as lexica. Computational
linguists have built and curated a trove of resources that store formally structured 
knowledge in machine-readable format: %treebanks \citep{marcus1993building}, 
thesauri \citep{borin2014bring}, knowledge bases \citep{miller1995wordnet}, 
and lexica \citep{gellerstam1999lexin}, among others, 
which have helped to develop countless NLP applications.
In this work, we show that it is possible to combine the structured information 
contained in a lexicon with the running text from which neural models traditionally 
learn semantic representations, and to derive word sense representations from those
separate sources of data.

% Swedish: the case of non-English NLP
All of the models presented in this thesis showcase their capabilities in the Swedish
language; not in vain, this work was developed at Spr{\aa}kbanken (the Swedish Language
Bank), a unit at the
Department of Swedish of the University of Gothenburg which devotes a large part of
its work in computational linguistics to developing resources for the Swedish language. Access to said resources
and expert advice is granted in such an environment, and it would be unreasonable 
not to take advantage from it. However, there is a conscious choice behind the 
development of these models in order for them to not be dependent on any specific
language: the
models we present here do not make any language-specific assumptions and so they
are able to learn from any language, provided that they are
fed with adequate data. This choice is made in the hope that our contribution is
maximally useful to the international community in which it has been nurtured. 

% BUT language agnoticism to show we are not living in a hole.

% TODO: Summarize: what we did, why we did it, what we accomplished
% Although the previous paragraph feels like a good section ending.
% And the summary in any case will come in Contributions, etc.

\section{Research questions}

This thesis work is mainly concerned with the creation of word sense semantic representations.
In particular, we are interested in applying neural network models to the task of
automatically learning those representations as their internal parameters. (See chapter
\ref{chapter:distributional} for detailed descriptions of neural network architectures for
this purpose.) We frame this task as an improvement over recent models dedicated to
learning word representations, or word embeddings; a specific characteristic of recent
word embedding models that has contributed to their successful implementation in NLP
systems, and that we would like to conserve in our models, is their computational 
efficiency in dealing with large amounts of textual data to achieve high quality 
representations.

\pagebreak
%As such, a preliminary version of our research question could be formulated as follows:
As such, we can formulate our first %main 
research question as follows:
\begin{quote}
{\bf Q1.} 
%Can embedding models be adapted to successfully transition from representing words to
%representing word senses, while keeping their computational efficiency?
Can embedding models be adapted to successfully transition from representing words to
providing separate representations for different senses of a word, while keeping 
their semantic representation capabilities and computational efficiency?
\end{quote}
Operationalizing this question requires us to test two characteristics of any model
proposed in this frame of reference: 
\begin{inparaenum}[(1)]
\item the quality of the word sense embeddings it learns, and
\item the computational overhead it would add relative to a comparable word embedding 
model.
\end{inparaenum}
Evaluating the quality of embeddings is a complex task which, on account of their
relative novelty, still lacks an evaluation standard accepted by the community. Usually,
test applications like word similarity are designed to assess the intrinsic 
quality of embeddings, while their extrinsic utility is tested on downstream 
applications like sentiment analysis. (See a detailed discussion about
evaluation techniques in chapter \ref{chapter:evaluation}.) The computational 
efficiency of embedding models can be measured, for example, as the amount of 
time they require to be trained under controlled conditions; training times of different
models can then be compared to give an assessment of their relative efficiency.

As mentioned before, it is our plan to include linguistic resources in these models. 
Specifically, our aim is to take advantage of knowledge about word senses encoded
in lexica through inventories of senses per word and lexical and semantic relations
between word senses to help steer the learning process of our models towards
representations of word senses that accurately portray lexicographic definitions
of senses. Thus, an addendum to question Q1 could be:
\begin{quote}
{\bf Q2.} Can the knowledge manually encoded in lexicographic resources be leveraged
to help improve representations learned by word sense embedding models trained on a
corpus? 
\end{quote}
The quality of embeddings emerges again as the core of this question, which makes it
necessary to assess the intrinsic and/or extrinsic performance of different models
so that their respective capabilities can be compared against each other, as explained
above. Ideally,
we should be able to measure the differences in performance between models that do not
use lexicographic knowledge as part of their training data and those that do so.
The formulation of question Q2 deliberately contrasts the different natures of
lexicographic and corpus data: while the latter consists mainly of unstructured
text in which the main assets are repetition and words acting as context for other words 
(see chapter \ref{chapter:distributional}), the former's strength
lies in carefully crafted structure and annotation rather than in the amount of data
and its distribution. 
Part of the answer to this question must thus examine the success in integrating the
two types of data, especially since we deal with models designed to work mainly with
the latter type. In order to do this, we need to be able to tell apart the influence
each type of data has on trained word sense embeddings and judge whether these 
influences contribute more or less equally to create sound meaning representations.
(See chapter \ref{chapter:a3} for examples of experiments addressing this specific 
issue.)

%TODO can this models help improve resources? 
Finally, we would also like to measure the value added by word sense representation
models to the community. One way to do so is to test the performance of embeddings on
downstream applications: for example, if precision scores on a %word sense disambiguation task
semantic frame prediction task (chapter \ref{chapter:a3})
rise when using word sense embeddings as features over using word 
embeddings while all other conditions remain equal, we can say that word sense embeddings
do add value to this task. Counting with word sense-dedicated representations also
might enable the use of embeddings as features in tasks like word sense disambiguation
or induction, where it is not as straightforward to apply them when the object represented
are word forms.
Using downstream applications to evaluate models provides
us then with a measure of added value. However, we could look at those same
lexical resources we propose to use for training our models as objects that could
also benefit from this work. Indeed, such resources are labor-intensive since they
require human input to be built, so any means of automation would simplify their
maintenance and expansion. Thus, we ask:
\begin{quote}
{\bf Q3.} How well suited are word sense embeddings to improve lexicographic resources? 
\end{quote}
Answering this question requires us to specify what {\em improving} means for a specific
resource. There exist several aspects of any lexicographic resource that might be 
improved, like coverage or correctness of existing content. For example, in chapter 
\ref{chapter:a4} we evaluate the capabilities of word sense embeddings to suggest 
new entries for a lexicon by selecting instances from a corpus that might contain
word senses not included in the lexicon, or in chapter \ref{chapter:a3} we try to
classify word senses into semantic frames for the Swedish FrameNet \citep{friberg2012rocky}. 
Designing such tasks as evaluation
strategies for our models allows us to measure their potential impact on resource building.

In summary, questions Q1, Q2, and Q3 encapsulate the goals of this thesis work, 
namely, to transition from word embedding models to word sense embedding models, to
enroll the help of lexicographic resources in this endeavor, and to measure the
capability of word sense embeddings to improve those resources. 
These questions also define the criteria by which we can measure the achievement of said
goals:
by assessing and comparing the intrinsic and extrinsic quality of embeddings learned
by different models, along with these models' computational efficiency; by testing
the integration and influence of different types of data sources that inform our models; and by
posing evaluation strategies that let us identify the potential applications that word 
sense embeddings have.

\section{Contributions}

The main contributions of this thesis are presented through a compilation of published
articles in part \ref{part:papers}. These comprise different models dedicated to 
automatically learning word sense semantic representations from corpora and lexica, along with 
evaluation methodologies intended to help determine the models' strengths and weaknesses.
The different models developed for this work are intended to explore the possibilities
at our disposal for distilling useful linguistic knowledge about word senses from 
existing resources and combining it with distributional data from corpora. 

In order to achieve that,
we work with a spectrum of the type of data used to train our models that ranges
from pure text from a corpus to lexical-semantic relations from a lexicon. Training
models on different points on this spectrum and assessing their performance on different
tasks allows us to 
\begin{inparaenum}[(1)]
\item determine the suitability of each type of data for the task of learning semantic
	representations for word senses, and
\item control the influence of each type of data on the resulting representations in
	order to establish the optimal proportion of each of them in terms of performance.
\end{inparaenum}
In particular, we present the following models:
\begin{enumerate}
	\item In article 1 \citep{nieto2015simple}, 
		contained in chapter \ref{chapter:a1},
		a model is introduced that learns word sense embeddings solely from
		a corpus with the exception that the number of senses for any given
		word is derived from a lexicon. This model is based on Skip-gram 
		\citep{mikolov2013efficient}, a word embedding model known for its
		computational efficiency; our modifications allow it to learn several 
		representations per word, while only introducing a 10\% computational
		overhead.
		Throughout this study we observe that
		such an approach is able to distinguish different meanings associated
		with the same word form and that these meanings correlate
		better with {\em word usage} rather than lexicographic senses; i.e.,
		the {\em word senses} of a word discovered by the model are differentiated 
		necessarily by the different contexts in which this word is used, since
		this is the only information available to the model.
	\item The other end of the spectrum is explored in article 2 \citep{nietopina2016embedding}
		(chapter \ref{chapter:a2}),
		where a model is trained to learn word sense embeddings from data
		generated from a lexicon. This model is presented along with a word
		sense disambiguation method based on the word sense embeddings learned,
		in an effort to showcase their utility on this task. This serves to 
		show that, even if the method does not reach state-of-the-art levels
		of performance, the type of model used, initially designed to be trained
		on corpora, is effectively able to extract useful 
		information about separation between senses of a word from lexicographic
		information. Furthermore, the disambiguation method presented is orders
		of magnitude faster than other graph-based methods.
	\item Having studied the prospects offered by each type of data, the middle
		ground of the spectrum is examined in article 3 \citep{nietopina2017training}, found
		in chapter \ref{chapter:a3}. 
		A new embedding model is presented here which is able to learn word sense
		representations jointly from textual and lexicographic data in adjustable
		proportions. Its aim is to put to work the lessons learned while designing
		the two previous models by trying to compensate one model's shortcomings 
		with the other's advantages. As it is possible to control the proportion
		of each type of data that feeds the model, we are able to find a balance
		between them and measure their impact on the results and we show with
		our evaluation strategy that
		what can be considered the ideal proportion for one specific downstream
		task may not be optimal for a different one.
\end{enumerate}

In addition to these models, we provide an extensive study of different evaluation
strategies that can be used to measure the quality of word sense embeddings. This
has proven to be a non-trivial endeavor for different reasons. (See chapter 
\ref{chapter:evaluation} for a detailed discussion on the topic.)  
On one hand, the very definition of {\em meaning of a word} is a contested issue
\citep{kilgarriff1997don,lenci2008distributional}, which in turns makes it difficult
to establish criteria for evaluating the quality of a semantic representation.
While a number of tasks like word similarity have been adopted as a standard to test
embedding models, these are usually geared towards word embeddings and are not
straightforward to adapt to the case of word sense embeddings. (See, for instance,
the approach taken to test word similarity by \citealp{neelakantan2014efficient}.)
On the other hand, a more pragmatic obstacle is the lack of resources to
be applied in evaluation. 
Many evaluation approaches involve comparing the results obtained by the system being
evaluated
against a standard which would be manually annotated or checked by humans. For example, in a 
word sense disambiguation task used for evaluation, the target words need the correct
disambiguation to be provided in order to check the quality of the automatic disambiguation
results. Such resources are not always readily available, especially for languages 
outside a small set of well resourced ones like English; this is the case for the 
Swedish language, which we used to build our semantic representations.

To counter these issues, we design evaluation plans that fit the model onto which
they are applied in terms of providing an accurate assessment of its characteristics,
in the hope that they may serve others in the community when presented with similar
challenges. A key point in achieving this is a complete coverage of a model's attributes
in the evaluation, so whenever possible we perform several assessment tasks on each
model that are used to inspect its different aspects. Qualitative assessments are
used to provide intuitive understanding of a model's capabilities, and quantitative
evaluation is performed through different tasks that measure a model's performance
in disparate scenarios; such tasks include comparison of sets of related terms in
a vector space versus a lexicon, similarity tests, word sense disambiguation, or
sentiment analysis.

Additionally, as one of our goals is to study the viability of automatically learned
semantic representations for improvement of resources, we provide a framework for
assessing word sense embeddings in this task. For this purpose, a system is developed
in article 4 \citep{nietopina2018automatically}, contained in chapter \ref{chapter:a4}, 
that extracts instances from a corpus containing word senses with a high probability
of not being listed in a lexicon, as a way of providing suggestions to lexicographers 
for expansion of the lexicon and partially automating their work. Furthermore, we 
show in chapter \ref{chapter:a3} the capacity of word sense embeddings to predict
membership of a term in a semantic frame of the Swedish FrameNet \citep{friberg2012rocky},
in such a manner that could be applied to add new entries to the knowledge base.
In article 5 \citep{borin2015dragons} (chapter \ref{chapter:a5}) we test the 
performance of different types of semantic
representations of senses on the task of linking entries in a modern lexicon with entries
in an older thesaurus, which serves to facilitate access and manipulation of an outdated
resource, as well as to pave the way for its potential expansion and modernization with
new entries from the contemporary lexicon. 

Finally, a word sense disambiguation mechanism based on word sense embeddings that was developed
during this thesis work has been incorporated to Sparv\footnote{\url{https://spraakbanken.gu.se/eng/research/infrastructure/sparv}} 
\citep{borin2016sparv},
Spr\aa kbanken's annotation tool. This disambiguation mechanism was introduced by
\cite{johansson2015combining}
and has been adapted for the evaluation of the models described in chapters \ref{chapter:a2}
and \ref{chapter:a3} of this text.
As part of Sparv's annotation pipeline, the mechanism is used to automatically
disambiguate and label instances of Swedish words in an input corpus, 
using a sense inventory obtained from the lexicon SALDO \citep{borin2013saldo}. 


%\section{Overview of publications}
% Redundant section covered by Contributions and Thesis structure.

%\section{Notation}
%TODO this (or chapter 3) might be a good place to discuss the terminogy differences 
%between embeddings, representations, vectors, etc.

\section{Thesis structure}

The rest of the text is structured as follows. Part \ref{part:intro}, which includes 
the current introductory chapter, sets the context for the work and gives background and detailed
descriptions of our models' main components. Chapter \ref{chapter:resources} formalizes
our working definition of word senses and discusses the types of resources on which 
our models are trained: lexica and corpora; chapter \ref{chapter:distributional} 
introduces the distributional hypothesis, then discusses distributional models for
obtaining word and word sense embeddings, as well as options available to introduce
lexicographic knowledge into them; chapter \ref{chapter:evaluation} reviews common evaluation
methods used on embedding models and describes the evaluation strategies we applied
on our models; finally, chapter \ref{chapter:summary} closes part \ref{part:intro}
with conclusions reached in this thesis.

Part \ref{part:papers} consists of a compilation of articles published during the
development of this thesis which contain the models and their applications that constitute
the core of the thesis work. Chapter \ref{chapter:a1} presents an unsupervised
model to learn word sense embeddings from corpora; as a counterpoint, chapter 
\ref{chapter:a2} introduces a model for learning word sense embeddings only from a 
lexicon which are applied to perform word sense disambiguation; chapter \ref{chapter:a3}
describes a joint approach to learning word sense embeddings from both a corpus
and a lexicon; chapter \ref{chapter:a4} explores the potential of linking word sense
embeddings with lexicon entries in order to find word senses not listed in the lexicon;
and chapter \ref{chapter:a5} investigates the applicability of word sense representations
to link entries of two different lexical resources in order to facilitate access
to and modernize outdated resources.
