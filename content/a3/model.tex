
\subsection{Learning word sense embeddings}
\label{sec:s2v}

  % Intro to Skip-Gram: objective, double vocabulary
  The Skip-gram word embedding model \citep{mikolov2013distributed} works on the
  premise of training the vector for a word $w$ to be able to predict those context words 
  $c_i$ with which it appears often together 
  in a large training corpus, according to the following objective function:
  \begin{displaymath}
    \sum_{i=1}^n \log p(c_i|w)
  \end{displaymath}
  where $p(c_i|w)$ can be approximated using the softmax function,
  %and word vectors are iteratively updated in order to maximize
  %their conditional log-probability. 
  The model, thus, works by maintaining two 
  separate vocabularies which represent word forms in their roles as \textit{target}
  and \textit{context} words. The resulting word embeddings (usually those vectors
  trained for the target word vocabulary) are able to store meaningful semantic
  information about the words they represent.

  % Modification to include word senses in one of the vocabularies
  The original Skip-gram model is, however, limited to word forms in both its
  vocabularies. \citet{nieto2015simple} introduced a modification of this model
  in which the target vocabulary holds a variable number of vectors for each word
  form, intended to represent its different senses. The training objective of such
  a model now has the following shape:
  \begin{equation}
    \log p(s|w) + \sum_{i=1}^n \log p(c_i|s)
    \label{eq:objective}
  \end{equation}
  Thus the word sense embeddings are trained to maximize the log-probability of
  context words $c_i$ given a word's sense $s$ plus the log-probability of that
  sense given the word $w$. For our purposes, this prior is a constant, 
  $p(s|w) = \frac{1}{n}$, as we do not have information on the probability of
  each sense of a given word.
  
  % On-line disambiguation
  This formulation requires a sense $s$ of word $w$ to be selected for each
  instance in which the objective function above is applied. This word sense 
  disambiguation is applied on-line at training time and based on the target
  word's context: The sense $s$ chosen to disambiguate an instance of $w$ is
  the one whose embedding maximizes the dot product with the sum of the context
  words' embeddings.
  \begin{equation}
    \arg \max_s \frac{e^{s \sum_i c_i}}{\sum_s e^{s \sum_i c_i}}
    \label{eq:wsd}
  \end{equation}

  This unsupervised model learns different usages of a word with minimal 
  overhead computation on top of the original, word-based Skip-gram. The
  number of senses per word can be obtained from a lexicon
  or set to a fixed number.

\subsection{Embedding a lexicon}

  In order to adapt the graph-structured nature of the data in an SN to
  be used in continuous representations, we propose to introduce it through a 
  regularizer that can act upon the same embeddings trained by the unsupervised
  model described above.

  Any given node $s$ in a graph will have a set of neighbors $n_i$ directly
  connected to it. In the graph underlying an SN, we assume $n_i$ to
  be lexically or semantically similar to $s$. In this setting, a collection 
  of sequences composed of word senses $s$ and $n_i$ can be
  collected by visiting all nodes in the SN's graph and collecting its
  immediate neighbors. Note that extracting such a
  collection of sequences from a semantic graph follows quite naturally,
  but in fact it could be generated from any other resource that relates concepts, 
  such as a thesaurus, even if it is not encoded in a graph, as long as the
  relations it contains are relevant to the model being trained.
  
  We propose to use a collection of sequences of related word senses to
  update their corresponding word sense vectors by pulling any two vectors closer together in their
  geometric space whenever they are encountered in a sequence. 
  This action can be easily modeled by minimizing the following expression:
  \begin{equation}
    \sum_{i=1}^k ||s - n_i||^2
    \label{eq:reg}
  \end{equation}
  for each sequence of word senses $(s, n_1, n_2, \ldots, n_k)$. By minimizing
  the distance in the vector space between vectors representing interconnected 
  concepts according to the SN's organization, the vector model is effectively
  representing that organization in a way that geometrical distance correlates
  with lexical or semantical relatedness, a central concept in the word embedding
  literature.

\subsection{Combined model}
  
  The two preceding sections describe the two parts of a combined model that
  is able to learn simultaneously from a corpus and an SN. This is achieved
  by training embeddings from a corpus with the objective described
  in equation~\ref{eq:objective}, and complementing this procedure with
  lexicographic data by means of using equation~\ref{eq:reg} as a regularizer.
  The extent of the regularizer's influence on the model is adapted by
  a mix parameter $\rho \in [0,1]$: the higher the value of $\rho$, the
  more influence the SN data has on the model, and vice versa.

  Thus, the objective function of our model is as follows:
  \begin{displaymath}
    \log p(s|w) + (1-\rho) \sum_{i=1}^n \log p(c_i|s) - \rho \sum_{j=1}^m ||s - n_j||^2
    \label{eq:comb}
  \end{displaymath}

  In practice, this objective is realized by alternating updates through
  each of the model's parts, the number of which is regulated by $\rho$.
  Updates on the corpus-based part are executed with Skip-gram with negative
  sampling \citep{mikolov2013distributed}, adapted to work with a vocabulary of
  word senses as explained in section \ref{sec:s2v}. 

  % Variations
  On top of the formulation of the lexicon-based part of the model given in the 
  previous section we propose two variations on this model
  in order to explore the extent to which the SN data can be used to influence
  the combined model explained in the following section. The initial formulation
  of the model will be referenced as V0 in this paper.
  
  In the first variation (henceforth V1) we propose to only apply equation~\ref{eq:reg} on 
  word senses pertaining to polysemous words. If by using the SN we intend
  to learn clear separations between different senses of a word, it attends
  to reason to limit its application to those cases, while monosemous words
  can be sufficiently well trained by the usual corpus-based approach, and
  act as semantic anchors in the broader vector space.

  The second variation (henceforth V2) deals with the specific architecture of the corpus-based 
  training algorithm. As mentioned in the previous section, this model trains
  a target and a context vocabulary. We propose to use the regularizer to act
  not only on word sense vectors, but also on context (word form) vectors. By
  doing this we expect the context vocabulary to be ready for instances of 
  different senses of a word, training context vectors to be potentially more 
  effective in the disambiguation scheme introduced in equation~\ref{eq:wsd}.
  This variation introduces an extra term into equation~\ref{eq:reg}, 
  \begin{displaymath}
    \sum_{i=0}^{n} ||w(s) - w(n_i)||^2
  \end{displaymath}
  where $w(x)$ is a mapping from a given sense $x$ to its corresponding word form.

