The recent success of word embeddings as effective semantic representations across
the broad spectrum of NLP tasks has led to an increased interest in developing
embedding methods further in order to acquire finer-grained representations able to handle 
polysemy and homonymy. This effort can be divided into two approaches: those that
tackle the problem as an unsupervised task, aiming to discover different usages of
words in corpora, and those that make use of knowledge resources as a way of injecting
linguistic knowledge into the models.

Among the earliest efforts in the former group is the work of \citet{reisinger2010multi} 
and \citet{huang2012improving}, who propose to cluster occurrences of words based
on their contexts to account for different meanings. With the advent of the Skip-gram
model \citep{mikolov2013distributed} as an efficient way of training prediction-based
word embedding models, much of the research into obtaining word sense representations revolved
around it. \citet{neelakantan2014efficient} and \citet{nieto2015simple} make use of 
context-based word sense 
disambiguation (WSD) during corpus training to allow on-line learning of multiple 
senses of a word with modified versions of Skip-gram. \citet{li2015multi} and 
\citet{bartunov2016breaking} apply 
stochastic processes to allow for representations of a variable number of senses per 
word to be learnt in unsupervised fashion from corpora. 

The embeddings obtained using this approach tend to be word-usage oriented, rather than
represent formally defined word senses. While this is descriptive of the texts in the corpus
at hand, it can be problematic for generalization. For instance, word senses that are 
underrepresented or absent in the training corpus will not be assigned a functional
embedding. On the other hand, due to the ability of these models to process large amounts
of data, well-represented word senses will acquire meaningful representations. 

The alternative approach to unsupervised methods is to include data from knowledge 
resources, usually graph-encoded semantic networks (SN) such as WordNet \citep{miller1995wordnet}.
\citet{chen2014unified} and \citet{iacobacci2015sensembed} propose to make use of
knowledge resources to produce a sense-annotated corpus, on which known techniques can
then be applied to generate word sense embeddings. A usual way of circumventing the lack
of sense-annotated corpora is to apply post-processing techniques onto pre-trained word embeddings
as a way of leveraging lexical information to produce word sense embeddings. The following
models share this method:
\citet{johansson2015embedding} formulate
an optimization problem to derive multiple word sense representations from word embeddings,
while \citet{pilehvar2016deconflated} and one of the models proposed by 
\citet{jauhar2015ontologically} use graph learning techniques to do so. 

A characteristic of this approach is that these models can generate embeddings for a 
complete inventory of word senses. However, the dependence on manually crafted resources
can potentially lead to incompleteness, in case of unlisted word senses, or to inflexibility 
in the face of changes in meaning, failing to account for new meanings of a word. 
%TODO From a technical point of view,  

The model that we present in this article tries to preserve desirable characteristics from 
both approaches. On one side, the model learns word sense embeddings from a corpus using
a predictive learning algorithm that is efficient, streamlined, and flexible with respect
to being able to discriminate between different usages of a word from running text. 
This learning algorithm is based on the idea of adding an extra latent variable to the Skip-gram
objective function to account for different senses of a word, that has been explored in previous
work by \citet{jauhar2015ontologically} and \citet{nieto2015simple}.
On the other side,
the learning process is guided by a regularizer function that introduces information from 
an SN, in an attempt to achieve a clear, complete, and fair division between the
different senses of a word. 
Furthermore, from a technical point of view, the effect of the regularizer function is
applied in parallel to the embedding learning process. This eliminates the need for a 
two-step training process or pre-trained word embeddings, and makes it possible to
regulate the influence that each source of data (corpus and SN) has on the learning 
process.
 

%Kiela et al., Specializing Word Embeddings for Similarity or Relatedness, EMNLP
%2015.

%Song et al, Sense Embedding Learning for Word Sense Induction, *SEM 2016.

