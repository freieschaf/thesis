
In our second evaluation, we investigated how well the sense vector models
learned by the different training algorithms correspond to semantic
classes defined by the Swedish FrameNet \citep{friberg2012rocky}. In a
frame-semantic model of lexical meaning \citep{fillmore2009frames}, 
the meaning of words is defined by associating them with broad
semantic classes called \emph{frames}; for instance, 
the word \emph{falafel} would belong to the frame \textsc{Food}.
%
Important classes of frames include those corresponding to objects and
people, mainly
populated by nouns, such as \textsc{Food} or \textsc{People\_by\_age};
verb-dominated frames corresponding to events, such as
\textsc{Impact}, \textsc{Statement}, or \textsc{Ingestion}; and
frames dominated by adjectives, often referring to relations,
qualities, and states, e.g. \textsc{Origin} or \textsc{Emotion\_directed}.

In case a word has more than one sense, it may belong to more than
one frame. In the Swedish FrameNet, unlike its English counterpart,
these senses are explicitly defined using SALDO
(see section \ref{sssec:saldo}): for instance, for the highly polysemous
noun \emph{slag}, its first sense (`type') belongs to the
frame \textsc{Type}, the second (`hit') to \textsc{Impact}, the
third (`battle') to \textsc{Hostile\_encounter}, etc.

In the evaluation, we trained classifiers to determine whether a 
SALDO sense, represented as a sense vector, belongs to a given frame
or not.
%
To train the classifiers, we selected the 546 frames from the Swedish
FrameNet for which at least 5 entries were available. In total we had  
28,842 verb, noun, adjective, and adverb entries, which we split
% TODO check this number
into training (67\% of the entries in each frame) and test sets (33\%).
%
For each frame, we used \textsc{Liblinear} \citep{fan2008liblinear} to train a
linear support vector machine, using the vectors of the
senses associated with that frame as positive training instances, and
all other senses listed in FrameNet as negative instances.

%
%Again, in a use case where a hard decision is needed rather than a
%ranking of potential LUs, we assign a threshold $b_F$ so that $s$ is
%assigned to $F$ if score$_F > b_F$. This threshold is also computed
%using \textsc{Liblinear}.
%
%Although we could imagine a rich feature set for
%describing LUs, we only considered vectors since our
%purpose is evaluation.
%The vectors were normalized to unit length before training the classifiers.
% normalized to unit length 

\begin{figure}[htbp]
\begin{center}

%\begin{adjustbox}{minipage=\linewidth,scale=0.7}
\begin{tikzpicture}
  \begin{axis}[legend pos=south west, 
      xmin=-0.1, xmax=1.1,ymin=0,ymax=55,
      xlabel=$\rho$, ylabel=MAP score
    ]
    % TODO *100
    \addplot [mark=none, black, dashed] {25.46};
    \addlegendentry{Lemma}
    \addplot table [x=RHO, y=VAN, col sep=comma, red, mark=square*] {content/a3/tab/fn_ap.csv};
    \addlegendentry{V0}
    \addplot table [x=RHO, y=POLY, col sep=comma, brown, mark=*] {content/a3/tab/fn_ap.csv};
    \addlegendentry{V1}
    \addplot table [x=RHO, y=CONT, col sep=comma, black, mark=asterisk] {content/a3/tab/fn_ap.csv};
    \addlegendentry{V2}
  \end{axis}
\end{tikzpicture}  
%\end{adjustbox}
\end{center}
%\vspace{-5mm}
\caption{MAP scores for the frame prediction classifiers for the different types of models.}
\label{fig:fneval}
\end{figure}

\subsubsection{Evaluation Results}

At test time, for each frame we applied the SVM scoring function of
its classifier to each sense in the test set. The ranking induced by
this score was evaluated using the Average Precision (AP) metric commonly
used to evaluate rankers; the goal of this ranking step is to score
the senses belonging to the frame higher than those that do not.
We computed the Mean Averaged Precision (MAP) score by macro-averaging
the AP scores over the set of frames.

Figure \ref{fig:fneval} shows the MAP scores of frame predictors based on
different sense vector models.
We compared the three training algorithms described in section \ref{sec:mod}
for different values of the regularization strength parameter $\rho$.
As a baseline, we included a model that does not distinguish between
different senses: it represents a SALDO sense with the word vector of
its lemma.

As the figure shows, almost all sense-aware vector models outperformed
the model that just used lemma vectors.
%
%
The result shows tendencies that are different from what we
saw in the WSD experiments.
%
The best MAP scores were achieved with mid-range values of $\rho$, so
it seems that this task requires embeddings that strike a balance
between representing the lexicon structure faithfully
and representing the cooccurrence patterns in the corpus.
A model with very light influence of the lexicon was hardly better than
just using lemma embeddings, and unlike what we saw for the WSD task
we see a strong dropoff when increasing $\rho$.

In addition, the tendencies here differ from the WSD results in that 
the training algorithm that only applies the lexicon-based regularizer to
polysemous words (V1) gives lower scores than the other two
approaches.
%
We believe that this is because it is crucial in this task that
sense vectors are clustered into coherent groups, which makes it more
useful to move sense vectors closer to their neighbors even when they
are monosemous; this as opposed to the WSD task, where it is more
useful to leave the monosemous sense vectors in place as ``anchors''
for the senses of polysemous words.
%
The context-regularized training algorithm (V2) gives no improvement
over the original approach (V0), which is expected 
since context vectors are not used in this task.

\begin{table}[htbp]
\centering
%\small
\begin{tabular}{lccc}
{\bf Frame} & {\bf Lemma} & {\bf V0} & {\bf V1} \\
\hline
\textsc{Animals} & 0.73 & \textbf{0.86} & 0.76 \\
\textsc{Food} & 0.72 & \textbf{0.84} & 0.77 \\
\hline
\textsc{Removing} & 0.20 & \textbf{0.50} & 0.22 \\
\textsc{Make\_noise} & 0.40 & \textbf{0.62} & 0.46 \\
\hline
\textsc{Origin} & 0.90 & \textbf{0.90} & 0.89 \\
\textsc{Color} & 0.73 & \textbf{0.88} & 0.80 \\
\hline
\textsc{Frequency} & 0.40 & \textbf{0.43} & 0.35 \\
\textsc{Time\_vector} & 0.40 & \textbf{0.52} & 0.27 \\
\end{tabular}
\caption{Frame prediction AP scores for selected frames dominated
by nouns, verbs, adjectives, and adverbs, respectively.}
\label{table:fnresults}
\end{table}

To get a more detailed picture of the strengths and weaknesses of the
models in this task, we selected eight frames: two frames dominated by
nouns, two for verbs, two for adjectives, two for adverbs.
%
Table \ref{table:fnresults} shows the AP scores for these frames of the
lemma-vector baseline, the initial approach (V0), and the
version that only regularizes senses of polysemous words (V1). All
lexicon-aware models used a $\rho$ value of 0.7.
%
Almost across the board, the V0 method gives very strong improvements.
The exception is the frame \textsc{Origin}, which contains adjectives
of ethnicity and nationality (\emph{Mexican}, \emph{African},
etc); this set of adjectives is already quite coherently clustered by
a simple word vector model and is not substantially improved by
any lexicon-based approach.

