Word embeddings, as a tool for representing the meaning of words based on the 
context in which
they appear, have had a considerable impact on many of the traditional Natural 
Language Processing tasks in recent years.
\citep{turian2010word, collobert2011natural, socher2011dynamic, glorot2011domain}
This form of semantic representation has come to replace in many instances 
traditional count-based vectors \citep{baroni2014don}, 
as they yield high-quality semantic representations in a 
computationally efficient manner, which allows them to leverage information
from large corpora.

Due to this success, some attention has been devoted to the question of whether their 
representational power can be refined 
to further advance the state of the art in
those tasks that can benefit from semantic representations.
%In this direction, and 
%given that a word can have more than one meaning, 
One instance in which this could be realized concerns polysemous words, which 
has led to several attempts at representing word senses instead of simple word forms. 
%TODO citations
Doing so would help avoid the situation in which several meanings of a word have to be
conflated into just one embedding, typical of simple word embeddings.

Among the different approaches to learning word sense embeddings, a distinction can be 
made between those that make use of a semantic network (SN) and those that do not.
%Some examples of the latter are the models introduced by 
%\newcite{reisinger2010multi}, \newcite{huang2012improving}, \newcite{neelakantan2014efficient}, 
%and \newcite{nieto2015simple}.
Approaches in the latter group usually apply an unsupervised strategy for clustering 
instances of words based on the 
context formed by surrounding words. The resulting clusters are then used to
represent the different meanings of a word. These representations characterize word usage
in the training corpus rather than lexicographic senses, and run the risk of marginalizing 
under-represented word senses. Nonetheless, for well represented word senses, this strategy
proves to be effective and adaptable to changes. 
%The resulting clusters are then used to 
%represent senses, which tend to be word 
%usage-oriented, rather than represent formally defined word senses.
%This characteristic, while descriptive of the texts in the corpus at hand,
%can be problematic for generalization. For instance, word senses that are 
%underrepresented or absent in the training corpus will not be assigned a functional 
%embedding. On the other hand, due to the ability of these models to process large
%amounts of data, well-represented word senses will acquire meaningful embeddings.

The alternative is to integrate an SN in the learning process. 
%TODO substitute SN by lexicon and make this explanation simpler?
% also the graph doesn't need to feature so prominently
This kind of resource
encodes a lexicon of word senses, connecting lexically and semantically related
concepts, usually in the form of a graph. 
%The strategies for including SNs in the process of learning embeddings
%are numerous, as there is no straightforward or standard manner of doing so.
%vary from simply supplying the number of senses any given word is expected to have,
%to performing complex analyses of the SN's underlying graph. 
%This is the approach taken by \newcite{chen2014unified},
%\newcite{jauhar2015ontologically}, 
%or \newcite{johansson2015embedding}, among others.
Methods that take this approach are able to work with lexicographic word senses as 
defined by experts, usually integrating them in different ways with corpus-learned 
embeddings. However, their completeness depends on the quality of the 
underlying SN. 
%Contrary to SN-free ones, these models can generate embeddings for a complete inventory
%of senses. However, they depend heavily on manually crafted resources, which could be
%potentially incomplete (missing word senses) or outdated (missing new meanings of a word).

% alternatively to the spin below, a clear way of presenting this is the merging of 
% two approaches, instead of (or on top of) the computational simplicity

%In many of the models that propose to leverage the information from an SN, the 
%integration is either too shallow, not making use of much of the information contained
%in the SN; or it requires considerable involvement at the
%cost of computational efficiency.

In this paper, we present an approach that tries to achieve a balance between these two variants. 
We propose to make use of an SN for learning word sense embeddings by 
leveraging its signal through a regularizer function that is applied on top
of a traditional objective function used to learn embeddings from corpora. 
In this manner, our model is able to merge these two opposed sources of data with the expectation 
that each one will balance the limitations of the other: flexible, high-quality embeddings 
learned from a corpus, with well defined separation between the expert-defined senses 
of any given polysemic
word. The influence of each source of information can be regulated through a mix parameter.
%generating corpus-like data
%from the SN's graph structure. By presenting the data in this manner to the
%learning algorithm, the training process works similarly to existing efficient
%simple word-based models. 

\pagebreak

As the corpus-based part of our model, we use a 
version of the Skip-gram \citep{mikolov2013distributed} 
model that is modified so that it is able to learn two distinct vocabularies: word senses and
word forms as introduced by \citet{nieto2015simple}. %TODO anonymity? 
Regarding the SN data, we focus our attention on its underlying graph. We assume that
neighboring nodes in such a graph correspond to semantically related concepts. Thus, given a 
word sense, a sequence of related word senses can be generated from its neighbors. 
A regularizer function can then be used to update their corresponding embeddings so that
they become closer in the vector space. This has the benefit of creating clear separations
between the different senses of polysemic words, precisely as they are described in the SN, 
even in the cases where this separation would not be clear from the data in a corpus.

%Furthermore, our model learns from two sources of data: the usual
%corpus and an additional dataset obtained from the SN. This dataset is created
%performing random walks (RWs) on the SN's graph \cite{nietopina2016embedding}: 
%a RW starting from a 
%given concept will emit a series word senses related to that concept. Our expectation 
%is that by enabling the model
%to learn from lexicographic data, it will learn more balanced word sense embeddings.
%This is achieved through encouraging the division of senses of any given word to
%stay close to that defined by lexicographers, rather than relying on word usage. 
%Additionally, our model improves the quality of problematic representations such
%as those for rare word senses which are underrepresented or completely absent in the 
%corpus.

We give an overview of related work in section~\ref{sec:relwor}, and our model 
is described in detail in section~\ref{sec:mod}. 
The resulting word sense embeddings are evaluated in section~\ref{sec:ev} on two separate automated 
tasks: word sense disambiguation (WSD) and lexical frame prediction (LFP).
The experiments used for evaluation allow us to investigate the influence of the 
lexicographic data on the embeddings by comparing
different model parameterizations. We conclude with a 
discussion of our results in section~\ref{sec:conc}.

