
%\maketitle

{\small
This chapter is a postprint version of the following publication:
\newline

\noindent Luis Nieto Pi\~{n}a and Richard Johansson 2016. 
Embedding senses for efficient graph-based word sense disambiguation. 
{\em Proceedings of TextGraphs-10: the Workshop on Graph-based Methods for Natural 
Language Processing, NAACL-HLT 2016,} 1–5. San Diego, USA.
}

%\begin{abstract}
\section*{Abstract}

We propose a simple graph-based method for word sense disambiguation (WSD) where
sense and context embeddings are constructed by applying the Skip-gram
method to random walks over the sense graph. 
We used this method to build a WSD system for Swedish using the SALDO
lexicon, and evaluated it on six different annotated test sets. In all
cases, our system was several orders of magnitude faster than a
state-of-the-art PageRank-based system, while outperforming a random baseline soundly.

%\end{abstract}

\section{Introduction}

Word sense disambiguation (WSD) is a difficult task for automatic systems
\citep{navigli2009word}. The most accurate WSD systems build on supervised
learning models trained on annotated corpora
\citep{taghipour2015onemillion}, but because of the difficulty of
the sense annotation task \citep{artstein2008inter}, the
luxury of supervised training is available for a few languages only.

%The scarcity 
%of sense-annotated corpora with which to develop new strategies to
%tackle the problem 
%only adds to the difficulty of finding effective solutions. 
%This scarcity is markedly patent when it comes to recent developments
%in language 
%modeling that benefit from using very large corpora. 
%\cite{collobert2011natural,sutskever2014sequence,jean2015using}

An approach that circumvents the lack of annotated corpora is to take
advantage of 
the information available in lexical knowledge bases
(LKBs) like WordNet \citep{miller1995wordnet,miller1998wordnet}.  %Corrected 2nd citation
This kind of resource encodes word sense lexicons as graphs connecting 
lexically and semantically related concepts. 
%
Several methods are available that use LKBs for WSD
\citep{navigli2007graph,agirre2009personalizing}. These approaches usually
apply a relatively complex analysis of the underlying graph based on the 
context of a target word to disambiguate it; e.g., \citet{agirre2009personalizing}
use the Personalized PageRank algorithm %\cite{brin1998anatomy}
to perform walks on the graph. %  achieving a high accuracy in their disambiguations.
However, these methods are computationally very costly, which makes
them practically useless for large corpora.

%In this paper we %propose to 
%investigate what a more time-efficient approach to LKB-based WSD
%is able to achieve. 

In this paper, we investigate a more time-efficient approach to
graph-based WSD. We represent the
concepts in the LKB by training vector space models on synthetic
datasets created using random walks on the LKB's graph. 
%
These synthetic datasets are built on the assumption that a random walk starting
at a given node in the graph will be composed of inter-related concepts,
effectively building a context for it. Training a vector space model on a collection
of such data generated for each node in an LKB's graph
would result in related concepts being represented near each other in the
vector space, according to the distributional hypothesis \citep{harris1954distributional}.
We then use these representations to perform context-based 
disambiguation taking advantage of the geometric notions of similarity
typical of vector space models. Using simple mechanisms for disambiguation and random walks
allows our method to be orders of magnitude faster while keeping its accuracy
well above the random-sense baseline.



%In the rest of this paper, we explain the details of our approach in section 2, 
%evaluate it on Swedish language data and compare its performance with related work 
%in section 3, and present our conclusions in section 4.

\section{Model}

\subsection{Word sense vector space model}

The Skip-gram model \citep{mikolov2013distributed} is a neural network language model
\citep{bengio2003neural} intended to produce high-quality word vector representations
trained on large collections of text. In its original formulation these representations
are limited to a vocabulary of word-forms extracted from the corpus used to train the
model. The representations are dense vectors in a high-dimensional space in which
it is expected that words with a similar meaning are represented near each other, 
which allows to associate 
a similarity measure with a geometrical distance measure. These representations
are trained to, given a word, predict its context; the training algorithm, thus, 
works with two separate vector spaces in which context and target words are represented.

% TODO The training objective explanation can be skipped if space is tight
%The training objective of the model is to optimize word representations for the task
%of predicting their context (i.e., words that that usually appear in their vicinity).
%This is achieved by maximizing the log-probability of the context words $c_i$ of a given
%target word $w$ repeatedly by scanning sentences in the training corpus:

%\begin{displaymath}
  %\sum_{i=1}^n \log p(c_i|w)
%\end{displaymath}

\pagebreak

Skip-gram introduced a highly efficient approach to language modeling
using a shallow neural architecture, which has also been extended to
handle word \emph{sense} representation 
\citep{neelakantan2014efficient,chen2014unified,johansson2015embedding,nieto2015simple}. Our aim in this paper is to build
\emph{graph-based} word sense embeddings and apply them to the task
of WSD as follows: Given %an unseen 
a sentence with an ambiguous word, we can then compare the
representation of its context words with each of the ambiguous word's sense representations
to decide which of them fits the context better.
%In order to do this, we need the model to be able to represent word senses
%as well as keeping its context vocabulary restricted to word-forms, to eventually output 
%two sets of vector representations: word senses and context words. 

For this purpose we use a modified version 
of the original Skip-gram implementation by \citet{levy2014dependency},
{\small\texttt{word2vecf}}, which 
specifies separate target and context vocabularies, making it possible to
represent word senses as targets while keeping  the context vocabulary restricted to word 
forms.

\subsection{Random walks as contexts}

% TODO Weighted vs unweighted graph

Given a node in a graph $G$, a random walk generates a random sequence of
interconnected nodes by 
%sampling from a uniform distribution over 
selecting randomly from
the edges of 
the current node at each step. The length of the random walk is controlled by
a stop probability $p_s$.  
I.e., at each node visited in the walk, the probability 
of stopping is $p_s$; if the walk does not stop, one of the node's edges is followed
to include another node in the walk. We repeat this process a number of times 
$N_{\text{walk}}$ 
for each node in $G$ to obtain $|G| \times N_{\text{walk}}$ random walks, where
$|G|$ is the number of nodes in $G$.

The nodes in $G$ are expected to represent word senses, while its edges connect
semantically related word senses. 
%e.g., possible connected nodes to \emph{to write} 
%would be \emph{pen}, \emph{publication}, \emph{writer}, \emph{to create}, etc. 
Thus, a sequence of nodes generated by a random walk is a set of 
related word senses. Our assumption is that such a sequence can be considered
a context of its starting node: a set of words that are related to, and can
appear together in real texts with, the word sense represented by that node, 
thus emulating real text sentences; 
to what extent this assumption holds depends of course on the 
structure of the LKB we are using.
Previous efforts in building word embeddings have shown the plausibility
of this approach \citep{goikoetxea2015random}.

It can also be argued that different senses of a word
appear in different contexts 
(e.g., it is plausible that the \emph{music} sense 
of \emph{rock} appears together with \emph{play} and \emph{concert}, while not so much
with \emph{mineral} or \emph{throw}). 
By generating contexts semantically related
to a given sense of a word, we expect the resulting vectors trained on them to be
effective in the task of word sense disambiguation. At the same time, as the same 
number of contexts (random walks) are generated for each word sense (node in $G$),
no word sense in the vocabulary contained in $G$ is under-represented, as can be the
case in real text corpora.

In order to conform to the definition of context vocabulary given above, given that 
nodes in $G$ represent senses, those senses
that form part of a context in a random walk will have to be mapped to their 
word-forms using a
dictionary.
%\subsection{Training the models}

%The data produced by the random walks needs to be further processed in order to
%be able to train the models. As explained above, we need a target vocabulary of
%word senses and a context vocabulary of word-forms to represent the contexts.
%Given a random walk, if we assume that the initial sense is the target and the
%rest of the senses are its context, the latter need to be mapped to their word-forms
%by using a dictionary.

\subsection{WSD mechanism}
\label{sec:wsd}

Given an ambiguous target word $w_i$ in context $c_{i,j}$, $j = 1, \dots, n$,
our disambiguation mechanism assigns a score to each of its senses $s_{i,k}$, 
$k = 1, \dots, K$, based on the dot product of the sense vector $v(s_{i,k})$
with the sum of the context vectors $v(c_{i,j})$:

\vspace{-2.5mm}
\begin{equation}
  v(s_{i,k})^{\intercal} \cdot \sum_{j=1}^n v(c_{i,j})
  \label{eq:score}
\end{equation}
\vspace{-2.5mm}

Note that all the information used to disambiguate originates from the LKB
in the form of co-occurrence of concepts in RWs on the graph; no \emph{external} 
information, like \emph{a priori} sense probabilities, are used. The scores in 
equation~\ref{eq:score} 
are derived from the probability of the context words given 
a sense, calculated using the softmax function:
\vspace{-1.5mm}
\begin{displaymath}
  p(c_{i,1}, \dots, c_{i,n} | s_{i,k}) = \frac{e^{v(s_{i,k})^{\intercal} \cdot \sum_{j=1}^n v(c_{i,j}) }}{\sum_{k' = 1 }^K e^{v(s_{i,k'})^{\intercal} \cdot \sum_{j=1}^n v(c_{i,j}) }}.
\end{displaymath}
\vspace{-3.5mm}

This expression is based on Skip-gram's objective
function used to maximize the probability of a context given a target word.
In our method, then, each ambiguous word is disambiguated by maximizing
its sense scores (Eq.~\ref{eq:score}) and selecting the highest scoring sense for that instance.

\section{Experiments}

We built a WSD system for Swedish by applying the random walk-based
training described above to the SALDO lexicon \citep{borin2013saldo}.
In the experiments, we then evaluated this system on six different
annotated corpora, in which the ambiguous words have been manually
disambiguated according to SALDO, and compared it to random and
first-sense baselines and UKB \citep{agirre2009personalizing}, a
state-of-the-art graph-based WSD system.

%The sense inventories in this annotation is defined
%by the SALDO lexicon \cite{borin2013}, and the graph defined by the
%lexical-semantic relations between senses in SALDO is the graph where we
%carry out the random walks used to train the sense and
%context vectors.

\subsection{The SALDO lexicon}

\input{content/a2/saldo}

\subsection{Evaluation corpora}
\label{sec:evalcorp}

\input{content/a2/corpora}

\subsection{Evaluation}

%The evaluation process consists of two steps: training the model 
%and applying the resulting representations to disambiguate the evaluation corpora.

A model is trained on synthetic datasets compiled from random walks on
SALDO. These walks are parameterized by their stop probability
$p_{\text{stop}}$, which effectively controls the length of the random walk and
has two effects: 
it impacts the size of training data (a lower $p_{\text{stop}}$ will generate longer
walks on average, and vice versa); 
and it controls the level of relatedness between the target sense and the words included 
in the context---a longer walk will wander away from the initial sense,
including increasingly unrelated concepts, while a shorter one will keep its concepts
closely related. 

We tuned the model by training several versions with different 
$p_{\text{stop}}$ and evaluated their performance on the development 
datasets. 
%The accuracies achieved by each of them are 
% in table~\ref{table:acc}. these accuracies are also 
%plotted against the corresponding random walk average lengths in 
%figure~\ref{fig:dev_cur}.
As the best-performing parameterization, we chose 
$p_{\text{stop}} = 0.25$, which generates random walks with an average length
of 3.75 nodes and achieves an accuracy of 51.6\% on the development datasets.
In all cases, the vector space's
dimensionality for senses and contexts is 200, and 10
iterations of the training algorithm are used.

Using this parameterization, we trained models on two different RW 
datasets: on one, random walks were performed on an unweighted version of SALDO
(i.e., all edges are equally probable from any given node);
on the other, the graph was weighted favoring the selection of a node's unique PD,
with probability 0.5, over inverse (incoming) PD connections, which were uniformly 
distributed over the remaining probability mass.  %TODO Delete line if necessary 

%The sense and context representations that result from training a model
%are used to calculate the dot product between a sense vector and the sum of
%its context word vectors to obtain a score. The context words are selected
%from a window of a fixed size; in these experiments we used a size of 10

%As explained above, the test datasets are composed of sentences that
%include an ambiguous word, for which the possible senses are given.
The disambiguation mechanism explained in section \ref{sec:wsd} 
is applied to sentences containing one ambiguous word 
using the sense and context representations that result from training the models:
A score is calculated for each of the senses of an ambiguous target word
in a context window of size 10 (to each side of the target word)
and the highest scoring sense is selected to disambiguate the entry.
The accuracy of the method is then obtained by comparing these
selections with the annotations of the test datasets.

%\begin{figure}
  %\centering
  %\includegraphics[scale=0.7]{figures/devel_curve}
  %\label{fig:dev_cur}
  %\caption{WSD accuracy on the development dataset corresponding to different random
  %walk average lengths in the training data.}
%\end{figure}

\pagebreak

The results of evaluating this models on each component of the test dataset
are shown in table~\ref{table:acc}. The performance of the UKB model\footnote{We used
  version 2.0 of UKB, run in the \emph{word-by-word} mode,
  using an unweighted graph based on the PD tree.} by 
\citet{agirre2009personalizing} on our datasets is also shown in this table,
along with first-sense (S1) and random-sense baselines (Rand).
These figures show that the first-sense approach is still a hard baseline.
Amongst our two models (RW), the one trained
on a weighted graph (w) performs consistently better; both of them outperform by a 
wide margin the random-sense baseline. 
The accuracy on the development sets is generally lower, especially in the case
of the first-sense baseline, underlying their difference in nature with respect
to the test sets (see section \ref{sec:evalcorp}).

\input{content/a2/results}

Regarding execution times, the tested models take a few hours to train and, once
trained, are able to disambiguate over 8\,000 instances per 
second, significantly surpassing the UKB model's times, which disambiguates 
approximately 8 instances per second. 
%Such a drastic speed-up 
%makes it feasible to apply our models in the processing of large corpora. 
This is related to the fact that the complexity of our disambiguation
mechanism is linear on the context vectors (see equation \ref{eq:score}), while
the UKB model's is dependent on the graph size.

\section{Conclusion}

In this paper we have presented a WSD method trained on a synthetic corpus
composed of random walks over an LKB's graph.
This method has been shown to be very efficient, disambiguating thousands
of words per second.
%Furthermore, its best results are obtained by training on relatively short 
%random walks, which helps speed up the time needed to generate a training
%dataset.
While the accuracy obtained by the method does not beat that of comparable 
approaches, it is several orders of magnitude faster while outperforming 
a random-sense baseline.
As has been shown in the results, the way in which random walks
are generated seems to have an influence on the results; exploring 
alternative ways of generating training datasets might be a way of 
improving the model's results while retaining its efficiency.

\section*{Acknowledgments}

This research was funded by the Swedish Research Council under
grant 2013--4944.
% \emph{Distributional methods to represent the meaning
%  of frames and constructions}, and grant 2012–5738, \emph{Towards a
%  knowledge-based culturomics}.
