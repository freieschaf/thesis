% TODO intro paragraph

% Ling resources provide us with data for training (and testing) our models.
% Different kinds of data: lex, cor, (benchmarks?)
Linguistic resources provide us with the data needed to train and test our representation
models. The kinds of data resources that we consider under this term are 
compiled (and possibly annotated) by computational
linguists to contain language samples and lexicographic inventories relating to one or
more languages; in particular, we are interested in lexica and corpora. Lexica provide
inventories of a language's vocabulary, while corpora contain samples of written text
intended to facilitate the conduction of linguistic analysis. In our models, we 
take advantage of that information to try and obtain semantic representations that
are derived automatically from those resources; also, in some instances, the performance of
these models is assessed with help of resources such as annotated corpora. 
(See chapter \ref{chapter:distributional}
for a description of different ways in which representational models learn from these
data resources, and chapter \ref{chapter:evaluation} for an account of how models
are evaluated using annotated data.)

% We also describe the linguistic unit that is the target of our research: word sense
%  and maybe link it to the resources introduced afterwards? 
In this chapter we also offer a description of the concept of word sense as used in
this thesis work. Word senses are the target of our research as the linguistic unit
for which we aim to create semantic representations. By processing the explicit
and implicit information about word senses present in linguistic resources, our 
models are able to learn to represent them in a vector space, providing us with
mathematically manipulable semantic objects easily handled by NLP applications.
% it is slippery a concept in NLP
%Such an object is a crucial %?
%tool to have in such applications that need to process meaning
Such applications that need to process meaning, like machine translation, 
sentiment analysis, or named entity recognition, among many others, rely on using
accurate semantic representations of the texts onto which they are applied.
The linguistic unit most commonly represented, however, is the word form;
since such representations are commonly obtained from corpora, composed of
text documents in a more or less unprocessed form, it is straightforward for this
to be the case. Nevertheless, employing one representation per word form may
conflate several meanings in the cases of words that have more than one, which has
the potential to damage the modeling power of the semantic vector space
\citep{neelakantan2014efficient,yaghoobzadeh2016intrinsic} and
the  performance of systems that work with semantic representations
\citep{li2015multi,pilehvar2016deconflated}. %,camacho2018word}.
%Our interest in word senses, then, is motivated by this issue. 
Our goal is to study
ways in which word sense representations can be derived from corpora and lexica
in order to obtain a more fine-grained representation of meaning that is oriented
towards representing distinct word senses rather than word forms.

% Motivation: why do we focus on senses? Why are they a problem worth considering?


\section{Word senses}
\label{sec:wordsenses}

%TODO some more reading on the topic would probably improve this section

%TODO make this description of representations properly into a section, possible towards
% the end of the chapter, or at least after  talking about word senses?
The larger part of this thesis work concerns the automatic creation of suitable 
representations for word senses.
While working at the word form level, any word $w$ is given a single representation
$v$; in many cases, such representations are vectors in a real-valued multidimensional
space, so that $v \in \mathbb{R}^N$. (See chapter \ref{chapter:distributional} for a
discussion on semantic representations.) However, linguistic phenomena like polysemy, by 
which a single word form is assigned more than one meaning, raise an issue with this
approach to semantic representation. For example, if the noun {\it rock} were represented
by a vector $v$, both of its two main meanings (`a mineral material' and `a type of music')
would share one representation. Conflation of the different senses of a word might
impact negatively the performance and quality of certain applications that use such representations 
\citep{li2015multi,yaghoobzadeh2016intrinsic}.
%TODO: if not done somewhere else (like ch. 3), go over the relevant results from these
%two papers here to explicitly lay out the consequences of meaning conflation
Our aim in these terms, then, is to devise ways in which a word $w$ with multiple
senses like {\it rock} can attain a separate representation $v_i$, $i \in 1, 2, \dots, n$,
for each of its $n$  senses.

% here explain only that a word gets a vector w while sense representations get w1, w2...
% and move all this bow stuff to chapter 3.

%here, relate sense definition to lexicographic definition of senses
%in order to explicitly mark what we mean by sense
%Done. Can it be improved? This is explicitly stated towards the end of this section.
%A word sense is one of the meanings associated to a particular word.
%as defined in a lexicographic inventory. 
%This term becomes relevant in the
%presence of linguistic phenomena such as polysemy or homonymy, by which one base
%form, or word, acquires more than one different meaning.
When a word can take several distinct meanings, through phenomena such as polysemy or 
homonymy, each of those meanings is known as a {\em word sense}.
E.g., {\it a small rodent} is one sense of {\it  mouse}, 
but another meaning
of the word is {\it a computer peripheral used to move a pointer on a screen}.
Given that there is no explicit indication of the intended meaning of an instance
of a polysemous word, {\em word sense disambiguation} has to be performed on it
in order to choose the word sense relevant for that occurrence and thus clarify its 
meaning. Such a process is informed by the context in which
that instance is found; i.e., the meaning contributed by words accompanying the 
ambiguous word in a sentence, a document, or a collection of documents.

Context is the main source of information for the task of 
disambiguating an instance of a word, both for humans and machines.
%humans are lexicographers here
%lexicographers may use this and that aspects of contexts to build curated lists
%detailing the meanings of a word
In order to prepare an inventory of word meanings for a lexicon, a lexicographer needs to
inspect the context of instances of each word in a corpus in order to categorize those 
instances into separate word senses. 
%The end result of this process depends heavily on the assumption that such a corpus
%contains a faithful representation of language; i.e., it has enough data to support
%the existence of all possible senses of each word.

%As a counterpart, machine approaches deal with several additional problems 
%which may lead to deviation from human-defined senses to a more usage based approach
Similarly, in machine-based approaches \citep{navigli2009word} 
to automatic discrimination of word senses (for the purpose of word sense disambiguation
or induction, for example,) data-driven techniques are usually deployed to compare contexts
of different instances of a word and classify them into word senses.

The result of any of these disambiguation processes, performed by humans or machines, relies on the 
assumption that the corpus employed contains a more or less faithful representation of
the language. 
This is so because any process of word sense discovery or disambiguation based on linguistic
evidence from a corpus will be affected by the sense inventory found in the corpus:
whether one particular word sense will result from such a process is subject to whether
the corpus contains enough evidence for it.
Especially in machine-based methods, where human insights into language are more
difficult to operationalize, the dependence on corpus evidence to track the different
meanings of a word tends to shift the concept of word sense towards word usage:
automatic disambiguation or discovery of word senses solely based on corpus data
gravitates towards identifying differences in usage of a word that may differ from lexicographic
word sense definitions of that same word. For example, consider the noun {\it mushroom}
to be defined in a coarse-grained lexicon as having a single meaning: {\it a fungal growth in the
shape of a domed cap on a stalk, with gills on the underside of the cap;} it is
conceivable that a process of automatic discovery of the word senses of {\it mushroom}
based on corpus evidence could conclude with the word having two senses derived
from two distinct contexts in which the word is commonly used: one pertaining to
biology, and another to culinary subjects. 
This disparity can potentially be addressed by
making lexicographic resources, such as lexica, available to the machine-based 
process in a way that the lexicographic descriptions of senses guide the sense
discovery process. 

Related to this, the granularity of word senses needs to be determined as a conscious
choice. In the example for {\it mushroom} above, the lexicographers in charge
of building that lexicon would have chosen it to be coarse-grained; it is entirely
reasonable that another, more fine-grained lexicon would separate the biological
and culinary meanings of {\it mushroom}. As an example of such discrepancies, 
\citet{palmer2007making} studied the differences in word sense granularity between
the sense inventories in the Senseval-1 \citep{kilgarriff2000framework} and Senseval-2 
\citep{edmonds2001senseval} tasks for automatic word sense
disambiguation (WSD): 
Senseval-1 obtains its sense inventory from the Hector lexicon \citep{atkins1992tools},
which results in the verbs used having 7.79 senses on average; on the other hand,
Senseval-2 extracts its English sense inventory from WordNet \citep{miller1995wordnet} 
(known for its high granularity,) which gives the verbs used an average of
16.28 senses. Such design decisions need to be taken into account whenever a sense
inventory is derived from a lexicon or other resource, since it will determine the
behavior of the system that inherits it.
%TODO unsupervised systems will also have some parameter controlling granularity.

%TODO find a better lead-in for this paragraph
Furthermore, the definition of word sense and its relation to word usage is 
not free of debate. For example, \citet{kilgarriff1997don} fails to find an operational definition
for {\em word sense} in the context of WSD, and concludes that a fixed, general-purpose
inventory of senses is not indicated for use in NLP applications; rather, word senses
would only be defined as they are needed by the application of interest and, thus, they
should emerge as abstractions of clusters of instances of word usage. 
%TODO an example or some clarification might be needed for this to be understood.
% and maybe a concluding sentence so that the next thing doesn't come out of the blue.
That is, it is his view that word senses exist only as clusters of instances of a word, and 
that such clusters are only defined on a {\it need-to-exist} basis dictated by the
task that calls for the clustering action. Thus, issues of completeness or granularity
are resolved by stating a set of task-specific clustering guidelines. This implies that
there cannot be a task-independent sense inventory.

While such ideas merit discussion, we intend to distance this work from theoretical debates
about the nature of word meaning. The question that guides this work is whether computational
models for meaning representation are able to capture different senses of a word and, in
particular, whether lexica can help in such a task. Thus, for the purpose of this
thesis, we consider a word sense for any given word when it is defined as such in the
lexicon. As a result, our computational models usually work with a fixed, discrete, and finite word sense
inventory that originates in the lexicon. In this context, we do not consider this a 
shortcoming since the lexicon's inventory is used as a gold standard for our models'
testing or training:
one form of model evaluation that we apply is to measure how well a model is able
to represent the inventory of senses found in the lexicon (chapter \ref{chapter:a1});
in other cases, the lexicographic sense inventory is used to steer the word sense 
learning process of the model (chapter \ref{chapter:a3}).
It is thus acknowledged that the lexicon used will have an influence on the results;
this is not necessarily a negative effect since our goal is not to obtain the ideal
sense inventory for a particular task but rather, given a sense inventory, find high-quality
representations for it.

%language evolves


\section{Lexica}

% Lexicon as the relation of words in a language
A lexicon is the collection of lexical items, represented by lemmas, of a language. It is
intended to function as a complete inventory of a language's main vocabulary and it 
can be complemented by additional information about its entries, such as 
their morphological characteristics or a structure of links between entries
that specify relations between them (e.g., {\em synonymy} relation between word senses which share
the same meaning, such as exists between {\it movie} and {\it film}; or 
{\em hypernymy-hyponymy} relation between a more general and a more specific term, such as
{\it plant} and {\it ivy}.)

Opposed to traditional lexical compilations, like dictionaries, which are built for human
consumption, 
modern lexica are intended for use in NLP processes, and store relevant lexical information 
in machine-readable formats that can effectively be used in such processes.
For example, the meanings of entries can be encoded by establishing links between them 
(which exploit semantic relations as mentioned above; see also WordNet below) so that
entries are defined in function of other entries; e.g., {\it car} is a {\em hyponym} 
of {\it vehicle}, and {\it tire}, {\it engine}, and {\it chassis} are all related to
{\it car} as being {\em parts of} it.
Entries in a lexicon
can also be decomposed into primitive concepts that clarify their meaning and allow to
relate different entries which share the same or a related meaning. For example, in a frame semantics
approach to building electronic lexical resources, word meaning is defined by assigning
words to semantic classes, or {\em frames}; in FrameNet \citep{baker1998berkeley}, one
such resource for English, {\it car} belongs to the frame {\scshape Vehicle}, and 
{\it engine}, {\it trunk}, and {\it seatbelt} belong to the frame {\scshape Vehicle\_subpart}.
These approaches to structuring lexical information are related to knowledge
bases, or ontologies, which are used to encode human general knowledge or domain-specific 
information for processing by computer
systems by structuring information via classes and subclasses linked by relations between
them. For an example of a general knowledge ontology, see Google's Knowledge Graph 
\citep{singhal2012introducing}.

Lexical resources also differ in the data and methods used for compiling them 
\citep{hazman2011survey}:
Obtaining lexical information from unstructured (corpora) or structured (databases) data,
via a manual process by lexicographers or an automatic method that leverages statistics
and patterns in the source data, or a semi-automatic method that filters the source
data for further processing by humans.


% Modern LT approaches for machine-ready lexica
Lexical resources have been the object of study and development on the field of
Language Technology since its early days \citep{reichert1969two,smith1973english}
with the goal of creating machine-readable resources that can be used to incorporate
lexical knowledge into NLP systems.
Computerized resources have the advantage of being able to store and process large
amounts of information, which allows them to be enriched with additional information
at a lower cost than their traditional, paper-based counterparts.
Abstract data types in Computer Science, such as graphs, also allow greater flexibility
in how the data is stored and used. These assets have been taken advantage of to
create large, wide-ranging lexical resources which contain substantial quantities
of information ready to be used for language processing.
An example of this is WordNet \citep{miller1995wordnet}, an English lexical database 
built as a graph connecting groups of synonyms ({\em synsets}) by means of 
semantic and lexical relations, such as hypernymy-homonymy. (See figure \ref{fig:wn} 
for a sample of WordNet's graph around the synset {\scshape Event}; relations in this graph
are indicated by directed arrows signaling the origin as a hypernym of the destination.)
\begin{figure}
\center
\includegraphics[scale=0.5]{content/2/fig/wn-sample}
\caption{A sample of WordNet's synset graph.}
\label{fig:wn}
\end{figure}

% The lexicon used in this work: SALDO - purpose, organization, characteristics
For our work on Swedish word sense representation, we have made use of such a resource
for the Swedish language: SALDO \citep{borin2013saldo}.

\subsection{A Swedish lexicon: SALDO}
\label{sec:saldo}

SALDO is a lexical-semantic network which, similarly to WordNet, represents concepts
in a graph's nodes and connects them using a variety of lexical-semantic relations.
The principles followed to build this network, however, are different from WordNet's
central concept of synonym sets. SALDO is organized as a hierarchy. Any of its entries,
has one or several {\em semantic descriptors} of which one is unique and mandatory: 
the primary descriptor. Semantic descriptors are also entries in the lexicon, so any
entry has at least one semantic descriptor, but can also be a semantic descriptor of
other entries. The characteristics of the relation formed between an entry and one 
of its semantic descriptors establishes SALDO's hierarchical structure. 

%TODO image example

In the case
of the {\em primary descriptor} (PD), an entry must be a {\em semantic neighbor} of, 
and more {\em central} than another in order to be its PD. 
Two entries in the lexicon are semantic neighbors when there exists a semantic 
relation between them, such as synonymy or hyponymy. 
Centrality is defined in terms of different criteria, such as frequency (words with
higher frequency are more central than words with lower frequency), stylistic value
(stylistically neutral words are more central than stylistically marked ones), 
derivation (words with lower derivational complexity are more central than those
with higher complexity), and type of relation in the case of asymmetrical relations
(e.g., a hypernym is more central than a hyponym). In practice, most PDs are
synonyms or hypernyms of the entry they describe. 

The stipulation by which any entry in SALDO must have one and only one PD (but can
potentially be PD of several other less central, semantically related entries) 
confers its underlying structure a tree architecture. This also implies that there
must be a root node, called PRIM, at the top of the PD hierarchy; this is an 
artificial entry created solely for this purpose, and bears no linguistic relation
to the entries of which it is a PD. (See a portion of SALDO's PD tree around
the term {\it music} in figure \ref{fig:saldo2}; relations in the tree are 
indicated by directed arrows signaling the PD of the arrow's origin.) 
\begin{figure}
\center
\includegraphics[scale=0.8]{content/2/fig/saldo}
\caption{A sample of SALDO's primary descriptor tree.}
\label{fig:saldo2}
\end{figure}

Other semantic descriptors are {\em secondary descriptors} (SD). An entry can
have more than one SD, and their chief purpose is to assist in 
describing the entry's meaning, especially in the case of its PD not being a synonym.
(Observe that in the case that the PD is a synonym, its semantic description is
rather complete.) There are no restrictions on the type of relation that must
exist between an entry and its SDs.

Each entry in SALDO is a sense of a word. A polysemous word, for instance, will
have one entry for each sense; e.g., the Swedish word {\it rock} is described as
having two meanings: `coat' and `rock music', so there are two entries, 
{\it rock\textsubscript{1}} and {\it rock\textsubscript{2}}, one for each sense
of {\it rock}.
Due to the principles followed for 
distinguishing senses to be included in this resource, SALDO's sense granularity
is coarser than that of WordNet. As described in its original formulation by
\citet{borin2013saldo}, the average number of senses for base forms in SALDO
is 1.1 and approximately 7\% of all base forms are polysemous, with the most
polysemous one having 10 senses; meanwhile in WordNet 17\% of base form-part
of speech combinations are polysemous, with the most polysemous one having
59 senses. Furthermore, entries in SALDO are not restricted to single-word
elements, but it also includes multi-word expressions. In addition to word
sense information, entries contain information about their part-of-speech and  
their inflectional pattern.

%TODO comment the implications of this granularity wrt our work (positive)

\section{Corpora}

% A corpus as a collection of text
A corpus is a collection of texts which, in the field of corpus linguistics, are
used to perform different kinds of linguistic analysis: gather statistics,
retrieve occurrences and linguistic evidence, or conduct comparative studies, among others. 
Modern corpora are stored in computer-readable form, so that tools developed by 
computational linguists can be applied onto them.
A corpus can have a general aim, by collecting texts from different types of 
sources, styles, and authors with the aim of providing a representative sample of
the language (or languages) covered; or it can have a narrow focus to enable the
study of a specific aspect of language, by sampling only texts relevant to the
subject: a historical period, a specific language variety, or a particular form
of online communication, for example. In the cases where corpora are used to train
language models or semantic representations, as is the case in several models presented
in this thesis, the selection of texts has an important influence over the resulting
models. As was discussed earlier in this chapter, such models learn the semantics of 
the language by analyzing its usage in text; it can be inferred from this that the
models trained on a corpus will reflect the language contained in it and, thus, this 
is a factor to be taken into account when choosing a corpus for these purposes. In our
work, we have striven towards representing language as used in a wide range of genres,
topics, registers, and styles in contemporary Swedish; to achieve this, we compiled
a training corpus from different sources in order to account for the desired variation 
(see below).

Besides differences in the language type
and topic covered, corpora may differ in a number of aspects that are defined
when a corpus is compiled, such as the size of included texts and the proportion of 
different text types, or what annotation and metadata are to be added onto the 
raw text, among others. A type of annotation of special interest for our work is
word-sense annotation, by which all or part of the words or lemmas contained in
a corpus are annotated with the sense corresponding to each instance, according to 
a pre-specified word sense inventory which can be extracted from a lexicon, or related
annotations such as semantic frames. Such corpora,
while laborious to produce due to the amount of human input needed, have an added
value for training and evaluating models such as are presented in this thesis, whose
main goal is to identify and represent word senses. For an example of a contemporaneous
corpus annotation effort which combines human input with help of language technology
tools, see the descriptions provided by \cite{johansson2016multi} for annotating
a Swedish corpus with word senses.

% The growth of written data in the age of the Internet
The use of the Internet by an ever increasing part of the population to communicate,
share knowledge and data, and access news and entertainment in the last decades
generates an extremely large amount of written language in the form of articles, 
blog posts, chat logs, product reviews among many others.
In the period from 1986 to 2007, \citet{hilbert2011world} estimated the growing global storage
capacity at 2.6, 15.8, 54.5, and 295 exabytes (1 EB equals $10^{18}$ bytes) in 1986, 1993, 
2000, and 2007, respectively; according to this same study, the proportion of these 
amounts of data stored in digital versus analog platforms grew from 25\% in 2000 to 
94\% in 2007.
Even if most of this vast amount of data is not textual (according to a \citet{cisco2013zettabyte}
white paper, 73\% of global IP traffic during 2016 was video traffic), the rapid growth and reach
of digital data also affects this medium.
Large collections of text available online have proven to be an invaluable source of data 
not only for the study of language itself, but for analyzing text-generating users' 
behavior from sociological and market points of view. The academic and industrial 
value of this data has in turn motivated the creation and refinement of language 
analysis tools able to leverage it. In summary, there currently exists a thriving
ecosystem revolving around corpora that enables acquisition of insight
from primary language data at an unprecedented level in terms of quantity, availability,
and analytic capacity.

\subsection{Swedish corpora used in this thesis}
% The corpus used in this work: characteristics, compilation, preprocessing?
For those models presented in this thesis that need a corpus to be trained on,
a Swedish language corpus is used consisting of approximately 1 billion 
words.
%\footnote{The corpus is available online at Spr\aa kbanken's website: 
%\url{http://spraakbanken.gu.se}.}

This corpus was compiled by aggregating a number of corpora\footnote{Available
for download at \url{https://spraakbanken.gu.se/eng/resources}.} featuring
different text sources in an attempt to achieve a balanced representation
of written Swedish language. It comprises text from social media
(corpora {\em Bloggmix 1998-2013; Twitter mix, August 2013; Swedish Wikipedia, August 2013}),
print and online newspaper texts ({\em DN 1987; GP 1994, 2001-2012; Press 65, 76, 95-98}),
texts from different science and popular science publications ({\em Forskning och 
framsteg; L\"{a}kartidningen 1996-2005; Smittskydd; Academic texts - Social science}),
fiction literature ({\em Bonniersromaner I, II; SUC novels}), and corpora with
mixed contents ({\em SUC 3; Parole}).

Furthermore, the texts in the corpus were tokenized, lemmatized, and POS-tagged using
Spr\aa kbanken's Korp NLP pipeline \citep{borin2012korp}. The tokenizer and lemmatizer
used are tools developed specifically for this pipeline, while the POS-tagger is HunPos 
\citep{halacsy2007hunpos}. Automatic segmentation of compounds was also applied on the
texts to split compound words into their components when a compound word's lemma was
not found in SALDO (see section \ref{sec:saldo}).

Besides the main corpus described above that was used to train our models,
we used a number of additional corpora in some of the evaluation tasks applied to test
the performance of models. In particular, these are corpora that include sense
annotations for all or part of their contents that we used for the purpose of solving
word sense disambiguation (WSD) tasks.

Two of these corpora were compiled collecting sentences used as glossing to illustrate
the use of Swedish word senses contained in the Swedish FrameNet \citep{friberg2012rocky}
and SALDO \citep{borin2013saldo}. These sentences have been selected by lexicographers
as examples of word sense usage for entries in those resources and, thus, contain
a word annotated with its sense each. In the case of the Swedish FrameNet glosses, 
a total of 1\,197 sentences were annotated in terms of its semantic frames (for which a mapping to SALDO
senses exist); the SALDO glosses correspond to 1\,168 sentences and are annotated with SALDO senses.

Another sense-annotated corpus was compiled with sentences from the Swedish Senseval-2
task \citep{kokkinakis2001senseval}. This collection contains 8\,237 sentences, originally 
divided into two subsets for training and testing. Each sentence contains an ambiguous
word, from a list of 40 possible words, annotated with its correct sense. In this case,
the word sense inventory used originally was obtained from the Gothenburg Lexical Database/Semantic
Database \citep{allen1981lemma}, but a manual mapping to SALDO word senses was used
to homogenize it with the rest of corpora \citep{nietopina2016benchmarking}; due to
the differences between sense inventories, the number of ambiguous words changed from 40 to
33.

Finally, the mixed-genre, sense-annotated corpus from the Koala annotation project 
\citep{johansson2016multi} was used. This corpus comprises seven sub-corpora
containing Swedish texts from different genres: blogs, novels, Wikipedia articles, European 
Parliament proceedings, political news, newsletters from a government agency, and
government press releases. The version we used (since the annotation project was
still ongoing at the time) was composed of 11\,167 sentences containing one 
sense-annotated word each, using the sense inventory from SALDO. The inter-annotator
agreement for two annotators on this corpus is given by a $\kappa$ coefficient \citep{cohen1960coefficient}
of 0.70 and an estimated agreement probability of 0.90.

%TODO Compound segmentation: what was the automatic segmentation algorithm?
