%\usepackage{times}
%\usepackage{url}
%\usepackage{latexsym}
%\usepackage{amsmath,amssymb}
%\usepackage{subcaption}
%\usepackage[utf8]{inputenc}
%\usepackage[ruled,noend,noline,linesnumbered]{algorithm2e}
%\usepackage{graphicx}
%\usepackage{float}
%\usepackage{paralist}

%\title{A Simple and Efficient Method to Generate Word Sense Representations}

%\author{Luis Nieto Piña \and Richard Johansson\\
  %Spr\aa kbanken, Department of Swedish, University of Gothenburg\\
  %Box 200, SE-40530 Gothenburg, Sweden\\
  %{\tt\{luis.nieto.pina, richard.johansson\}@svenska.gu.se}
%}

%\makeatletter
%\newcommand{\removelatexerror}{\let\@latex@error\@gobble}
%\makeatother

%\begin{document}
%\maketitle

%\begin{abstract}

{\small
This chapter is a postprint version of the following publication:
\newline

\noindent Luis Nieto Pi\~{n}a and Richard Johansson 2015. A simple and efficient method to generate
word sense representations. {\em Proceedings of the International Conference Recent 
Advances in Natural Language Processing,} 465–472. Hissar, Bulgaria. 
}

\section*{Abstract}
Distributed representations of words have boosted the performance of many Natural
Language Processing tasks. However, usually only one representation per word is 
obtained, not acknowledging the fact that some words have multiple meanings. 
This has a negative effect on the individual word representations
and the language model as a whole. In this paper we present a simple model
that enables recent techniques for building word vectors to represent distinct
senses of polysemic words. In our assessment of this model 
we show that it is able to effectively discriminate between words' senses
and to do so in a computationally efficient manner.

%\end{abstract}

\section{Introduction}
\label{sect:intro}

Distributed representations of words have helped obtain better language 
models \citep{bengio2003neural} and improve the performance of many 
natural language processing applications such as named entity 
recognition, chunking, paraphrasing, or sentiment 
classification \citep{turian2010word,socher2011dynamic,glorot2011domain}.
Recently, the 
Skip-gram model \citep{mikolov2013efficient,mikolov2013distributed} was proposed, 
which is able to produce high-quality representations
from large collections of text in an efficient manner.

Despite the achievements of distributed representations, polysemy or homonymy are 
usually disregarded even when word semantics may have a large influence 
on the models. This results in several distinct senses of one same word 
sharing a representation, and possibly influencing the representations of 
words related to those distinct senses under the premise that similar
words should have similar representations. Some recent attempts to address
this issue are mentioned in the next section.

%There have been some recent attempts to address this issue. 
%\newcite{reisinger2010multi} propose clustering feature vectors corresponding
%to occurrences of words into a predetermined number of clusters,
%whose centers provide multiple representations for each word,
%in an attempt to capture distinct usages of that word.
%\newcite{huang2012improving} cluster context vectors instead, built as 
%weighted average of the words' vectors that surround a certain target word.
%\newcite{neelakantan2014efficient} import the idea of clustering context vectors
%into the Skip-gram model.

We present a simple method for obtaining sense representations
directly during the Skip-gram training phase. It differs from 
most previous approaches in that it does not need to create 
or maintain clusters to discriminate between senses, leading to a significant
reduction in the model's complexity. It also uses a heuristic approach to 
determining the number of senses to be learned per word that allows the model
to use knowledge from lexical resources but also to keep its ability to work
withouth them. In the following sections we look at previous work, describe
our model, and inspect its results in qualitative and quantitative evaluations.

\section{Related work}

One of the first steps towards obtaining word sense embeddings was that by
\citet{reisinger2010multi}. The authors propose to cluster occurrences 
of any given word in a corpus into a fixed number $K$ of clusters which
represent different word usages (rather than word senses). Each word's
is thus assigned multiple prototypes or embeddings.

\citet{huang2012improving} introduced a neural language model that leverages 
sentence-level and document-level context to generate word embeddings.
Using the approach by \citet{reisinger2010multi} to generate multiple  
embeddings per word via clusters and training on a corpus whose words have been substituted
by its associated cluster's centroid, the neural model is able to learn
multiple embeddings per word.

\citet{neelakantan2014efficient} tried to expand the Skip-gram model 
\citep{mikolov2013efficient,mikolov2013distributed} to produce word sense 
embeddings using the clustering approach
of \citet{reisinger2010multi} and \citet{huang2012improving}. Notably, Skip-gram's
architecture allows the model to, given a word and its context, select and train a 
word sense embedding jointly. The authors also introduced a \emph{non-parametric} variation
of their model which allows a variable number of clusters per word instead of a fixed $K$.

\pagebreak

Also based on the Skip-gram model, \citet{chen2014unified} proposed to maintain and
train context word and word sense embeddings conjunctly, by training the model to
predict both the context words and the senses of those context words given a
target word. To avoid using cluster centroids to represent senses, the number of sense 
embeddings per word and their initial values are obtained from a knowledge 
network.

Our system for obtaining word sense embeddings also builds upon the Skip-gram model
(which is described in more detail in the next section). Unlike most of the models
described above, we do not make use of clustering algorithms. We also allow each word 
to have its own number of senses, which can be obtained from a dictionary or using
any other heuristic suitable for this purpose. These characteristics translate into
\begin{inparaenum}[\itshape a\upshape)] 
  \item little overhead calculations added on top of the initial word-based model; and 
  \item an efficient use of memory, as the majority of words are monosemic.
\end{inparaenum}

\section{Model description}
\label{sect:model}

\subsection{From word forms to senses}

The distributed representations for word forms that stem from a Skip-gram
\citep{mikolov2013efficient,mikolov2013distributed} model are built 
on the premise that, given a certain target word, they should serve to predict its 
surrounding words in a text. I.e., the training of a Skip-gram model, given a 
target word $w$,
is based on maximizing the log-probability of the context words of $w$, $c_1, \dots, c_n$:

%OK
\begin{equation}
\label{eq:sg_obj}
\sum_{i = 1}^{n} \log p(c_i | w).
\end{equation}

The training data usually consists of a large collection of sentences or documents, so that 
the role of target word $w$ can be iterated over these sequences of words, while the context words $c$
considered in each case are those that surround $w$ within a window of a certain length. The objective
then becomes maximizing the average sum of the log-probabilities from equation~\ref{eq:sg_obj}.

We propose to modify this model to include a sense $s$ of the word $w$. Note that
equation~\ref{eq:sg_obj} equals
%OK
\begin{equation}
\label{eq:sg_obj2}
\log p(c_1, \dots, c_n | w)
\end{equation}
if we assume the context words $c_i$ to be independent of each other given a target 
word $w$. The notation in equation~\ref{eq:sg_obj2} allows us to consider the Skip-gram 
as a Na\" ive Bayes	model parameterized by word	embeddings \citep{mnih2013learning}.
In this scenario, including a sense would amount then to adding a latent variable $s$, 
and our model's behaviour given a target word $w$ is to select a sense $s$, 
which is in its turn used to predict $n$ context words $c_1, \dots, c_n$. Formally:
%ok
\begin{equation}
\label{eq:nb}
\begin{aligned}
p(s,c_1, \dots, c_n | w) = & \\
p(s|w) \cdot p(c_1, \dots, c_n | s) = & \\ 
p(s|w) \cdot p(c_1|s) \dots p(c_n|s).
\end{aligned}
\end{equation}
Thus, our training objective is to maximize the sum of the log-probabilities of 
context words $c$ given a sense $s$ of the target word $w$ plus the log-probability 
of the sense $s$ given the target word:

%ok
\begin{equation}
\label{eq:obj}
\log p(s|w) + \sum_{i = 1}^{n} \log p(c_i | s).
\end{equation}

We must now consider two distinct vocabularies: $V$ containing all 
possible word forms (context and target words),	and $S$ containing 
all possible senses for the words in $V$, with 	sizes $|V|$ and
$|S|$, resp. Given a pre-set $D \in \mathbb{N}$, our ultimate goal 
is to obtain $|S|$ dense, real-valued vectors of dimension $D$ that 
represent the senses in our vocabulary $S$ according to the objective 
function defined in equation~\ref{eq:obj}. 

The neural architecture of the Skip-gram model works with two separate 
representations for the same vocabulary of words. This double 
representation is not motivated in the original papers,	but it stems 
from \verb+word2vec+'s code\footnote{\url{http://code.google.com/p/word2vec/}} 
that the model builds separate representations for context and target words, 
of which the former	constitute the actual output of the system.
(A note by \citealp{goldberg2014word2vec} offers some insight into this subject.)  %TODO fix this arxiv citation
We take advantage of this architecture and use one of these two representations 
to contain senses, rather than word forms: as our model only uses target 
words $w$ as an intermediate step to select a sense $s$, we only do not need 
to keep a representation for them. In this way, our model builds a representation 
of the vocabulary $V$, for the context words, and another for the vocabulary 
$S$ of senses, which contains the actual output. Note that the representation 
of context words is only used internally for the purposes of this work, and 
that context words are word forms; i.e., we only consider senses for the 
target words.
	
	
\subsection{Selecting a sense}
\label{sec:sense}

In the description of our model above we have considered that for each 
target word $w$ we are able to select a sense $s$. We now explain the 
mechanism used for this purpose. The probability of a context word $c_i$
given a sense $s$, as they appear in the model's objective function
defined in equation~\ref{eq:obj}, $p(c_i|s)$, $\forall i \in [1,n]$, can be 
calculated using the \textit{softmax} function:
\begin{displaymath}
\label{eq:softmax}
p(c_i|s) = \frac{e^{v_{c_i}^{\intercal} \cdot v_{s}}}
		{\sum_{j=1}^{|V|} e^{v_{c_j}^{\intercal} \cdot v_{s}}}
	 = \frac{e^{v_{c_i}^{\intercal} \cdot v_{s}}}{Z(s)},
\end{displaymath}
where $v_{c_i}$ (resp. $v_s$) denotes the vector representing context word $c_i$ 
(resp. sense $s$), $v^{\intercal}$ denotes the transposed vector $v$, and in the 
last equality we have used $Z(s)$ to identify the normalizer over all context words.
With respect to the probability of a sense $s$ given a target word $w$, for simplicity 
we assume that all senses are equally probable; i.e., $p(s|w) = \frac{1}{K}$ for any 
of the $K$ senses $s$ of word $w$, $\text{senses}(w)$.

% algorithm2e version
%\begin{figure*}[!t]
%%\removelatexerror
%%\IncMargin{1em}
%\begin{algorithm}[H]
  %\small
  %\renewcommand{\AlCapSty}[1]{\normalfont\normalsize{\normalfont{#1}}}
%\DontPrintSemicolon
%\KwIn{Sequence of words $w_1, \dots, w_N$, window size $n$, 
      %learning rate $\alpha$, number of negative words $N_{neg}$}
%\KwOut{Updated vectors for each sense of words $w_i$, $i = 1, \dots, N$}
%%\KwResult{how to write algorithm with \LaTeX2e }

%\For{$t = 1, \dots, N$}{
    %$w = w_i$ \;
    %$K \leftarrow$ number of senses of $w$ \;
    %context($w$) = $\{ c_1, \dots, c_n \, | \, c_i = w_{t + i}, \,\, i = -n, \dots, n, \,\, i \neq 0 \}$ \;
    %\For{$k = 1, \dots, K$}{
	    %$p_k = \frac{e^{(v_{c_1} + \cdots + v_{c_n}) \cdot v_{s_k}}}
	     %{\sum_{j = 1}^K e^{(v_{c_1} + \cdots + v_{c_n}) \cdot v_{s_j}}}$ \;
    %}
    %$s = \arg\,\max_{k = 1, \dots, K} \, p_k$ \;
    %\For{$i = 1, \dots, n$}{
	    %$f = \frac{1}{1 + e^{v_{c_i} \cdot v_s}}$ \;
	    %$g = \alpha(1 - f)$ \;
	    %$\Delta = g \cdot v_{c_i} $\;
	    %$v_{c_i} = v_{c_i} + g \cdot v_s$ \;
	    %\For{$j = 1, \dots, N_{neg}$}{
		    %$d_j \leftarrow$ word sampled from noise distribution, $d_j \neq c_i$ \;
		    %$f = \frac{1}{1 + e^{v_{d_j} \cdot v_s}}$ \;
		    %$g = -\alpha \cdot f$ \;
		    %$\Delta = \Delta + g \cdot v_{d_j}$ \;
		    %$v_{d_j} = v_{d_j} + g \cdot v_s$ \;
	    %}
	    %$v_s = v_s + \Delta$ \;
    %}
%}

%\caption{Selection of senses and training using Skip-gram with Negative Sampling. 
%(Note that $v_x$ denotes the vector representation of word/sense $x$.)}
%\label{alg:s2v}
%\end{algorithm}
%%\DecMargin{1em}
%\end{figure*}

\begin{algorithm}[]

\begin{algorithmic}%[1]
\Require{Sequence of words $w_1, \dots, w_N$, window size $n$, 
      learning rate $\alpha$, number of negative words $N_{neg}$}
\Ensure{Updated vectors for each sense of words $w_i$, $i = 1, \dots, N$}
%\KwResult{how to write algorithm with \LaTeX2e }

\For{$t = 1, \dots, N$}
    \State{$w = w_t$} 
    \State{$K \leftarrow$ number of senses of $w$} 
    \State{context($w$) = $\{ c_1, \dots, c_n \, | \, c_i = w_{t + i}, \,\, i = -n, \dots, n, \,\, i \neq 0 \}$} 
    \For{$k = 1, \dots, K$}
        \State{$p_k = \frac{e^{(v_{c_1} + \cdots + v_{c_n}) \cdot v_{s_k}}}
        {\sum_{j = 1}^K e^{(v_{c_1} + \cdots + v_{c_n}) \cdot v_{s_j}}}$} 
    \EndFor 
    \State{$s = \arg\,\max_{k = 1, \dots, K} \, p_k$} 
    \For{$i = 1, \dots, n$}
        \State{$f = \frac{1}{1 + e^{v_{c_i} \cdot v_s}}$} 
        \State{$g = \alpha(1 - f)$} 
        \State{$\Delta = g \cdot v_{c_i} $}
        \State{$v_{c_i} = v_{c_i} + g \cdot v_s$} 
	\For{$j = 1, \dots, N_{neg}$}
	    \State{$d_j \leftarrow$ word sampled from noise distribution, $d_j \neq c_i$} 
	    \State{$f = \frac{1}{1 + e^{v_{d_j} \cdot v_s}}$} 
	    \State{$g = -\alpha \cdot f$} 
	    \State{$\Delta = \Delta + g \cdot v_{d_j}$} 
	    \State{$v_{d_j} = v_{d_j} + g \cdot v_s$} 
	\EndFor 
	\State{$v_s = v_s + \Delta$} 
    \EndFor 
\EndFor
\end{algorithmic}

\caption{Selection of senses and training using Skip-gram with Negative Sampling. 
(Note that $v_x$ denotes the vector representation of word/sense $x$.)}
\label{alg:s2v}
\end{algorithm}
%\DecMargin{1em}
%\end{figure*}

Using Bayes formula on equation~\ref{eq:nb}, we can now obtain the 
posterior probability of a sense $s$ given the target word $w$ and the context 
words $c_1, \dots, c_n$:
%ok
\begin{equation}
\label{eq:posterior}
\begin{aligned}
p(s|c_1, \dots, c_n, w) = \\
 \frac{p(s|w) \cdot p(c_1, \dots, c_n|s)}
        {\sum_{s_k \in \text{senses}(w)}p(s_k|w) \cdot p(c_1, \dots, c_n| s_k)} = \\
	\frac{e^{(v_{c_1} + \, \cdots \, + v_{c_n}) \cdot v_s} \cdot Z(s)^{-n}}
        {\sum_{s_k \in \text{senses}(w)}
	e^{(v_{c_1} + \, \cdots \, + v_{c_n}) \cdot v_{s_k}} \cdot Z(s_k)^{-n}}.
\end{aligned}
\end{equation}
During training, thus, given a target word $w$ and context words $c_1, \dots c_n$, 
the most probable sense $s \in \text{senses}(w)$ is the one that maximizes 
equation~\ref{eq:posterior}. Unfortunately, in most cases it is computationally 
impractical to explicitly calculate $Z(s)$. From a number of possible approximations, 
we have empirically found that considering $Z(s)$ to be constant yields 
the best results; this is not an unreasonable approximation if we expect the context
word vectors to be densely and evenly spread out in the vector space. 
Under this assumption, the most probable sense $s$ of $w$ is the one that maximizes
%ok
\begin{equation}
\label{eq:posterior2}
\frac{e^{(v_{c_1} + \cdots + v_{c_n}) \cdot v_s}}
{\sum_{s_k \in \text{senses}(w)} e^{(v_{c_1} + \cdots + v_{c_n}) \cdot v_{s_k}}}
\end{equation}
For each word occurrence, we propose to select and train only its most probable sense.
This approach of \textit{hard sense assignments} is also taken in \citet{neelakantan2014efficient}'s work %TODO fix this citation if possible (genitive + subject instead of object)
and we follow it here, although it would be interesting to compare it with a \emph{soft}
updates of all senses of a given word weighted by the probabilities obtained with equation~\ref{eq:posterior}.

The training algorithm, thus, iterates over a sequence of words, selecting each one
in turn as a target word $w$ and its context words as those in a window of a 
maximum pre-set size. For each target word, a number $K$ of senses $s$ is considered, 
and the most probable one selected according to equation~\ref{eq:posterior2}. (Note that, 
as the number of senses needs to be informed -- using, for example, a lexicon --
monosemic words need only have one representation.) The selected sense
$s$ substitutes the target word $w$ in the original Skip-gram model, and any of the
known techniques used to train it can be subsequently applied to obtain sense representations.
The training process is drafted in algorithm~\ref{alg:s2v} using Skip-gram with Negative Sampling.

Negative Sampling \citep{mikolov2013distributed}, based on Noise Contrastive Estimation 
\citep{mnih2012fast}, is a computationally efficient approximation for the original Skip-gram objective 
function (equation~\ref{eq:sg_obj}). In our implementation it learns the sense representations by 
sampling $N_{neg}$ words from a noise distribution and using logistic
regression to distinguish them from a certain context word $c$ of a target word $w$. 
This process is also illustrated in algorithm~\ref{alg:s2v}.	

 
\section{Experiments}
\label{sect:eval}

We trained the model described in section \ref{sect:model} on Swedish text using a context
window of 10 words and vectors of 200 dimensions.
%
The model requires the number of senses to be specified for each word;
as a heuristic, we used the number of senses listed in the SALDO
lexicon \citep{borin2013saldo}. Note, however, that such a resource is not vital
and could be substituted by any other heuristic. E.g., a fixed number of 
senses per word, as \citet{neelakantan2014efficient} do in their parametric approach.

As a training corpus, we created a
corpus of 1 billion words downloaded from Spr{\aa}kbanken, the Swedish language
bank.\footnote{\url{http://spraakbanken.gu.se}} The corpora are
distributed in a format where the text has been tokenized,
part-of-speech-tagged and
lemmatized. Compounds have been segmented automatically and when a
lemma was not listed in SALDO, we used the parts of the compounds
instead. The input to the software computing the embeddings
consisted of lemma forms with concatenated part-of-speech tags,
e.g. \emph{dricka}-verb for the verb `to drink' and \emph{dricka}-noun
for the noun `drink'.

The training time of our model on this corpus was 22 hours. For the sake of
time performance comparison, we run an off-the-shelf \verb+word2vec+ 
execution on our corpus using the same parameterization described above; 
the training of word vectors took 20 hours, which illustrates
the little complexity that our model adds to the original Skip-gram.

\subsection{Inspection of nearest neighbors}

We evaluate the output of the algorithm qualitatively by inspecting
the nearest neighbors of the senses of a number of example 
words, and comparing them to the senses listed in SALDO. 
%We leave a
%quantitative evaluation to future work.

Table \ref{table:nns} shows the nearest neighbor lists of the senses
of two words where the algorithm has been 
able to learn the distinctions used in the lexicon.
The verb \emph{flyga} `to fly' has two senses listed in SALDO: to travel
by airplane and to move through the air.
The adjective \emph{öm} `tender' also has two senses, similar to the
corresponding English word: one emotional and one physical.
%
The lists are semantically coherent, although we note that they are
topical rather than substitutional; this is expected since the algorithm was
applied to lemmatized and compound-segmented text and we use a fairly
wide context window.

\begin{table}[htbp]
\centering
\begin{subtable}{.9\linewidth}
\centering
%\footnotesize
\begin{tabular}{ll}
{\bf Sense 1} & {\bf Sense 2} \\
\hline
\emph{flyg} `flight' & \emph{flaxa} `to flap wings' \\
\emph{flygning} `flight' & \emph{studsa} `to bounce' \\
\emph{flygplan} `airplane' & \emph{sväva} `to hover'\\
\emph{charterplan} `charter plane' & \emph{skjuta} `to shoot'\\
\emph{SAS-plan} `SAS plane' & \emph{susa} `to whiz'
\end{tabular}
\caption{\emph{flyga} `to fly'}
\end{subtable}%
\vspace{5mm}
\begin{subtable}{.9\linewidth}
\centering
%\footnotesize
\begin{tabular}{ll}
{\bf Sense 1} & {\bf Sense 2} \\
\hline
\emph{kärleksfull} `loving' & \emph{svullen} `swollen' \\
\emph{ömsint} `tender' & \emph{ömma} `to be sore' \\
\emph{smek} `caress' & \emph{värka} `to ache'\\
\emph{kärleksord} `word of love' & \emph{mörbulta} `to bruise'\\
\emph{ömtålig} `delicate' & \emph{ont} `pain'
\end{tabular}
\caption{\emph{öm} `tender'}
\end{subtable}

\caption{Examples of nearest neighbors of the two senses of two
  example words.}
\label{table:nns}
\end{table}

In a related example, figure~\ref{fig:asna} shows the projections onto
a 2D space\footnote{The projection was computed using \texttt{scikit-learn} 
\citep{pedregosa2011scikit} using multidimensional scaling of the distances in a 
200-dimensional vector space.} of the representations for the two senses of 
\emph{åsna}: 'donkey' or 'slow-witted person', and those of their corresponding 
nearest neighbors.

\begin{figure}
\centering
\fbox{\includegraphics[scale=0.65]{content/a1/fig/asna2}}
\caption{2D projections of the two senses of \emph{åsna} 
('donkey' and 'slow-witted person') and their nearest neighbors.}
\label{fig:asna}
\end{figure}

For some other words we have inspected, we fail to find one or more of
the senses. This is typically when one sense is very dominant,
drowning out the rare senses. For instance, the word \emph{rock} has
two senses, `rock music' and `coat', where the first one is much more
frequent. While one of the induced
senses is close to some pieces of clothing, most of its nearest
neighbors are styles of music.

In other cases, the algorithm has come up with meaningful sense
distinctions, but not exactly as in the lexicon.
%
For instance, the lexicon lists two senses for the noun \emph{böna}:
`bean' and `girl'; the algorithm has instead created two bean senses:
bean as a plant part or bean as food.
%
In some other cases, the algorithm finds genre-related distinctions instead
of sense distinctions. For instance, for the verb \emph{älska},
with two senses `to love' or `to make love', the algorithm has found
two stylistically different uses of the 
first sense: one standard, and one related to informal words
frequently used in social media. Similarly, for the noun \emph{svamp}
`sponge' or `mushroom'/`fungus', the algorithm does not find the
sponge sense but distinguishes taxonomic, cooking-related, and
nature-related uses of the mushroom/fungus sense.
%
It's also worth mentioning that when some frequent foreign word is
homographic with a Swedish word, it tends to be assigned to a sense.
For instance, for the adjective \emph{sur} `sour', the
lexicon lists one taste and one chemical sense; the algorithm
conflates those two senses but creates a sense for the French
preposition.
%

\subsection{Quantitative evaluation}

Most systems that automatically discover word senses have been
evaluated either by clustering the instances in an annotated corpus 
\citep{manandhar2010semeval,jurgens2013semeval}, or by measuring the effect
of the senses representations in a downstream task such as contextual
word similarity \citep{huang2012improving,neelakantan2014efficient}. 
%\cite{pantel2002}
However, Swedish lacks sense-annotated corpora % but Senseval???
as well as word similarity test sets, so our evaluation is instead based on
comparing the discovered word senses to those listed in the SALDO
lexicon. We selected the 100 most frequent two-sense nouns, verbs, and
adjectives and used them as the test set.

To evaluate the senses discovered for a lemma, we generated two sets
of word lists: one derived from the lexicon, and one from
the vector space. For each sense $s_i$ listed in the lexicon, we
created a list $L_i$ by selecting
the $N$ senses (for other words) most similar to $s_i$ according to the
graph-based similarity metric by \citet{wu1994verb}. Conversely, for each
sense vector $v_j$ in our vector-based model, a list $V_j$ was built
by selecting the $N$ vectors most
similar to $v_j$, using the cosine similarity. We finally mapped the
senses back to their corresponding lemmas, so that the two sets 
$L = \{ L_i \}$ and $V = \{V_j\}$ 
of word lists could be compared.

%To exemplify, the noun \emph{hopp} has two senses, one meaning
%\emph{hope} and the other \emph{jump}. If we set $N$ to 5, the two
%lexicon-derived lists are:

%\begin{enumerate}
%\item \emph{hoppfullhet} `hopefulness', \emph{panik} `panic',
%\emph{desperation} `desperation', \emph{defaitism} `defeatism',
%\emph{hysteri} `hysteria'
%\item \emph{backhoppning} `ski jump', \emph{volt} `somersault',
%  \emph{simhopp} `dive', \emph{stavhopp} `pole vault', \mbox{\emph{upphopp} `up-jump'}
%\end{enumerate}

%\begin{enumerate}
%\item \emph{hoppare} `jumper', \emph{ansatsbana} `xxx',
%  \emph{stavhopp} `pole vault', \emph{upphopp} `up-jump'}
%
%\item \emph{hoppfullhet} `hopefulness', \emph{panik} `panic',
%\emph{desperation} `desperation', \emph{defaitism} `defeatism',
%\emph{hysteri} `hysteria'
%\end{enumerate}

These lists were then evaluated using standard clustering evaluation
metrics. We used three different metrics:

\begin{itemize}%\addtolength{\itemsep}{-0.5\baselineskip}

\item 
\emph{Purity/Inverse-purity F-measure} \citep{zhao2001criterion},
where each of the lexicon-based lists $L_i$ is matched to the vector-based
list $V_j$ that maximizes the $F$-measure, the harmonic mean of the
cluster-based precision and recall:
\[
\begin{array}{cc}
P(V_j, L_i) = \frac{|V_j \cap L_i|}{|C_j|} & %\mbox{ } &
R(V_j, L_i) = \frac{|V_j \cap L_i|}{|L_i|} 
\end{array}
\]
The overall $F$-measure is defined as the weighted average of individual $F$-measures:
\[
F = \sum_i \frac{|L_i|}{\sum_k |L_k|} \max_j F(V_j, L_i)
\]
%where 
%\[
%P(C_j, L_i) = \frac{|C_j \cap L_i|}{|C_j|} \mbox{\hspace{2mm}} 
%R(C_j, L_i) = \frac{|C_j \cap L_i|}{|L_i|} 
%\]

\item 
\emph{B-cubed F-measure} \citep{bagga1998algorithms}, which computes individual
  precision and recall measures for every item occurring in one of the lists,
  and then averaging all precision and recall values. 
%Using the
%formulation of \newcite{amigo2009}, for two items $e$ and $e'$, we define
%
%  {\small
%  \[
%  \mbox{Corr}(e, e') = \left\{ 
%\begin{array}{ll}
%1 & \mbox{iff} L(e) = L(e') \leftrightarrow C(e) = C(e')} \\
%0 & \mbox{otherwise}
%\end{array}
%\right.
%  \]
%}
The $F$-measure
is the harmonic mean of the averaged precision and recall. 

\item 
\emph{V-measure} \citep{rosenberg2007measure}, the harmonic mean of the
\emph{homogeneity} and the \emph{completeness}, two entropy-based metrics.
The homogeneity is defined as the relative reduction of entropy in $V$
when adding the information about $L$:
\[
h(V, L) = 1 - \frac{H(V|L)}{H(V)}
\]
Conversely, the completeness is defined
\[
c(V, L) = 1 - \frac{H(L|V)}{H(L)}.
\]
Both measures are set to 1 if the denominator is zero.

\end{itemize}%

Table \ref{table:eval} shows the results of the evaluation for nouns,
verbs, and adjectives, and for different values of the list size $N$. 
As a
strong baseline, we also include an evaluation of the sense representations
discovered by the system of \citet{neelakantan2014efficient}, run
with the same settings as our system. 
This system is available only in its parametric version. (I.e., the number
of senses per word is a fixed parameter.) As the words used in the experiments
always have two senses assigned, this parameter is set to 2. This accounts
for fairness in the comparison with our approach, which is given the \emph{right}
number of senses by the lexicon (and thus in this case also 2). 
%
We used the three metrics mentioned above: Purity/Inverse-purity
F-measure (\emph{Pu-F}), B-cubed F-measure (\emph{B$^3$-F}), and
V-measure (\emph{V}).
%
As we can see, our system achieves higher scores than the baseline in
almost all the evaluations, despite using a simpler algorithm that
uses less memory. Only for the
$V$-measure the result is inconclusive for verbs and adjectives; for
nouns, and for the other two evaluation metrics, our system is
consistently better.

\begin{table}[htbp]
  %\small
  \centering
%\begin{center}
%\begin{small}
\begin{subtable}{\linewidth}
  \centering
%\setlength\tabcolsep{1.8mm}
\begin{tabular}{c|cc|cc|cc}
\multicolumn{1}{c}{} & \multicolumn{2}{c}{\textit{Pu-F}} & \multicolumn{2}{c}{\textit{B$^3$-F}} & \multicolumn{2}{c}{\textit{V}} \\
$\boldsymbol{N}$ & {\bf N-14} & {\bf ours} & {\bf N-14} & {\bf ours} & {\bf N-14} & {\bf ours} \\
\hline
10 & {9.4} & \textbf{10.7} & {2.5} & \textbf{2.8} & {8.9} & \textbf{10.6} \\
20 & {9.5} & \textbf{10.8} & {2.1} & \textbf{2.4} & {6.7} & \textbf{8.9} \\
40 & {9.0} & \textbf{9.9} & {1.8} & \textbf{2.0} & {5.1} & \textbf{7.2} \\
80 & {7.8} & \textbf{8.9} & {1.4} & \textbf{1.7} & {4.3} & \textbf{5.6} \\
160 & {7.4} & \textbf{8.2} & {1.3} & \textbf{1.5} & {3.9} & \textbf{4.7} \\
\end{tabular}
\caption{Nouns.}
\vspace{5mm}
\end{subtable}
\begin{subtable}{\linewidth}
  \centering
%\setlength\tabcolsep{1.8mm}
\begin{tabular}{c|cc|cc|cc}
\multicolumn{1}{c}{} & \multicolumn{2}{c}{\textit{Pu-F}} & \multicolumn{2}{c}{\textit{B$^3$-F}} & \multicolumn{2}{c}{\textit{V}} \\
$\boldsymbol{N}$ & {\bf N-14} & {\bf ours} & {\bf N-14} & {\bf ours} & {\bf N-14} & {\bf ours} \\
\hline
10 & {9.1} & \textbf{10.8} & {2.0} & \textbf{2.5} & \textbf{11.3} & {7.6} \\
20 & {8.1} & \textbf{9.3} & {1.4} & \textbf{1.7} & {6.7} & \textbf{7.5} \\
40 & {7.3} & \textbf{8.2} & {1.0} & \textbf{1.3} & {4.5} & \textbf{4.5} \\
80 & {7.5} & \textbf{8.7} & {1.0} & \textbf{1.3} & \textbf{3.7} & {3.2} \\
160 & {8.2} & \textbf{10.3} & {1.2} & \textbf{1.7} & {1.2} & \textbf{1.5} \\
\end{tabular}
\caption{Verbs.}
\vspace{5mm}
\end{subtable}
\begin{subtable}{\linewidth}
  \centering
%\setlength\tabcolsep{1.8mm}
\begin{tabular}{c|cc|cc|cc}
\multicolumn{1}{c}{} & \multicolumn{2}{c}{\textit{Pu-F}} & \multicolumn{2}{c}{\textit{B$^3$-F}} & \multicolumn{2}{c}{\textit{V}} \\
$\boldsymbol{N}$ & {\bf N-14} & {\bf ours} & {\bf N-14} & {\bf ours} & {\bf N-14} & {\bf ours} \\
\hline
10 & {6.8} & \textbf{7.6} & {1.4} & \textbf{1.7} & {9.4} & \textbf{10.7} \\
20 & {6.5} & \textbf{7.6} & {1.3} & \textbf{1.5} & \textbf{8.5} & {7.2} \\
40 & {6.4} & \textbf{7.3} & {1.1} & \textbf{1.3} & {5.4} & \textbf{5.8} \\
80 & {6.5} & \textbf{7.0} & {1.0} & \textbf{1.1} & \textbf{5.2} & {4.7} \\
160 & {6.9} & \textbf{7.5} & {1.0} & \textbf{1.1} & {4.1} & \textbf{4.4} \\
\end{tabular}
\caption{Adjectives.}
\end{subtable}
%\end{small}
%\end{center}
\caption{Evaluation of the senses produced by our system and that of  \citet{neelakantan2014efficient}.}
\label{table:eval}
\end{table}

\section{Conclusions and future work}

In this paper, we present a model for automatically building sense 
vectors based on the Skip-gram method. 
In order to learn the sense vectors, we modify the Skip-gram model
to take into account the number of senses of each target word. By including 
a mechanism to select the most probable sense given a target word 
and its context, only slight modifications to the original training 
algorithm are necessary for it to learn distinct representations of 
word senses from unstructured text.

To evaluate our model we train it on a 1-billion-word Swedish corpus
and use the SALDO lexicon to inform the number of senses associated to
each word. Over a series of examples in which we analyse the nearest neighbors
of some of the represented senses, we show how the obtained sense 
representations are able to replicate the senses defined in SALDO, or to make
novel sense distinctions in others. On instances in which a sense is 
dominant we observe that the obtained representations favour this sense
in detriment of less common ones.

We also give a quantitative evaluation of the sense representations
learned by our model using a variety of clustering evaluation metrics, and 
compare its performance with that of the model proposed by 
\citet{neelakantan2014efficient}. In most instances of this evaluation
our model obtains higher scores than this baseline, despite its relative 
lower complexity. Our model's low complexity is characterized by 
\begin{inparaenum}[\itshape a\upshape)]
  \item the simple
word sense disambiguation algorithm introduced in section~\ref{sec:sense},
which allows us to fit word sense embeddings into Skip-gram's existing
architecture with little added computations; and 
  \item the flexible number of 
senses per word, which takes advantage of the monosemic condition of 
most words to make an efficient use of memory. 
\end{inparaenum}
This low complexity is
demonstrated by our training algorithm's small increase in running time 
with respect to that of the original, word-based Skip-gram model.

In this work, our use of a lexicon is limited to setting the number of senses 
of a given word, 
While this information proves useful for obtaining coherent sense 
representations, an interesting line of research lies in further 
exploiting existing knowledge resources for learning better sense vectors. 
E.g., leveraging the network topology of a lexicon such as 
SALDO, that links together senses of semantically 
related words, could arguably help improve the representations for those 
rare senses with which our model currently struggles, by learning their
representations taking into account those of neighbour senses in the network.

\section*{Acknowledgments}

We would like to thank the reviewers for their constructive comments.
This research was funded by the Swedish Research Council under
grant 2013--4944, \emph{Distributional methods to represent the meaning
  of frames and constructions}. 
