\subsection{Lexicon}

A lexicon which lists word senses and provides relations
between them is required; for instance, a resource 
that encodes these relations in a graph architecture, such as WordNet 
\citep{miller1998wordnet}.  % Fixed citation
%concepts in the lexicon are stored in nodes and closely related ones share
%edges which might be labelled with the type of relation between them.

We need to retrieve word senses related to any given target word sense in order to
obtain a set of \textit{neighbors} which put the target sense in context. This context
will be used to compare it with sets of neighbors extracted from a word sense vector
space for senses of the same lemma, and thus find the best match to establish a link. 
%This comparison allows to find the best match 
%between pairs of \textit{neighborhoods} to establish a link between the two.

In our experiments on Swedish data we use SALDO \citep{borin2013saldo}
as our lexicon. It is encoded as a network:  
nodes encode word senses, which are connected by edges defined by semantic 
\textit{descriptors}: the meaning of a sense is defined by one or more senses, its
descriptors. Among others, each sense has one \textit{primary descriptor} (PD) 
and, in turn, it may be 
the primary descriptor of one or more senses. E.g., the PD of the
musical sense of \textit{rock} would be \textit{music}, which would be the 
PD of \textit{hard rock}.  
The PD network of SALDO traces back to a \textit{root node} (which does not have
a PD) and, thus, it has a tree topology.
In general, senses with more abstract meanings are closer
to the root, while more concrete ones are located closer to the leaves. 
%For our
%purposes, we do not make a distinction between different types of relationships.
%, which means
%that when sets of neighbors are collected, both parent and children relations are
%considered equally.


\subsection{Word sense embeddings}

Word sense embeddings allow us to create an analogy between semantic relatedness
among words and geometric distance between their representations in a vector space.
In particular, a word sense embedding model assigns multiple 
representations to any given word, each of which is related to a distinct word sense.
For our purposes, we make use of Adaptive Skip-gram \citep{bartunov2016breaking}, 
an adaptation of the hierarchical softmax Skip-gram \citep{mikolov2013distributed}.
%This model automatically assigns a number of representations to a word based on its 
%usage in a corpus by means of a Bayesian approach.

%In particular, this model builds on the hierarchical softmax variation of 
%Skip-gram. 
The hierarchical softmax model is described by its training objective: to maximize the probability
of context words $v$ given a target word $w$ and the model's parameters $\theta$:
\begin{equation}
p(v | w, \theta) = \prod_{n \in \text{path}(v)} 
\frac{1}{1 - e^{-\text{ch}(n) x_w^\top y_n}},
\label{eq:hs}
\end{equation}
where $x_w$ are the input representations of the target words $w$, and the 
original output representations of context words $y_n$ are associated with nodes in 
a binary tree which has all possible vocabulary words $v$ as leaves; $\theta$ is the
set of these representations as weights of the vector model. In this 
context, $\text{path}(v)$ are the nodes $n$ in the path from the tree's root to the 
leaf $v$, identified by $\text{ch}(n)$ being -1 or 1 depending on whether $n$ is a 
right or left child.

The Adaptive Skip-gram (AdaGram) model expands this objective to account for 
multiple word senses to be represented in the input vocabulary $X$. It does so
by introducing a latent variable $z$ that determines a concrete sense $k$ of word $w$.
The output vocabulary $Y$ remains unchanged.
%\begin{displaymath}
%p(v | z=k, w, \theta) = \prod_{n \in \text{path}(v)} 
%\frac{1}{1 - e^{-\text{ch}(n) \text{x}^\top_{wk} \text{y}_n}}
%\end{displaymath}
This model uses a Dirichlet process (\textit{stick-breaking representation})
to automatically determine the 
number of senses per word. 
They define a prior over the multiple senses of a
word as follows:
\begin{displaymath}
p(z = k | w, \boldsymbol{\beta}) = \beta_{wk} \prod_{r=1}^{k - 1} (1 - \beta_{wr}),
\end{displaymath}
\begin{displaymath}
p(\beta_{wk} | \alpha) = \text{Beta}(\beta_{wk} | 1, \alpha),
\end{displaymath}
where $\boldsymbol{\beta}$ is a set of samples $\beta$ from the Beta distribution
and $\alpha$ is a hyperparameter. By combining this prior with a pre-specified
maximum number of prototypes and a threshold probability, the model is able to
automatically determine the number of word senses any given word is expected to
have.
The objective function that defines the AdaGram model is defined as follows:
\begin{equation}
%\begin{split}
p(Y,Z,\boldsymbol{\beta} | X, \alpha, \theta) = 
		      \prod_{w=1}^V \prod_{k=1}^\infty p(\beta_{wk} | \alpha) %\\
		      \prod_{i=1}^N \left[ p(z_i | x_i, \boldsymbol{\beta})
		      \prod_{j=1}^C p(y_{ij} | z_i, x_i, \theta) \right],
%\end{split}
\end{equation}
where $V$ is the word vocabulary size, $N$ is the size of the training corpus, 
$C$ is the size of the context window, and $p(\beta_{wk} | \alpha)$ is the prior
over multiple senses obtained via the Dirichlet process 
described above.
%Note that this allows for an infinite number of senses per word.
%In practice, and given that the number of senses for any given word is limited by 
%the use of finite training corpora, a ceiling on this number is used as one of the
%model's parameters. 
The granularity in the distinction between word senses is 
controlled by $\alpha$. A trained model produces representations for word senses
in a $D$-dimensional vector space where those with similar meanings are closer together than
those with dissimilar ones. 
%Similarity calculations can thus be conducted using vector operations
%which can be used, for example, to disambiguate an instance of a word in context by
%measuring the similarity of each of its sense vectors to the context words.

For the purposes of this work, we trained a word sense embedding model on a Swedish corpus (cf. 
section~\ref{sec:corpus}) using the default parameterization of AdaGram: 100-dimension
vectors, maximum 5 prototypes per word, $\alpha=0.1$, and one training epoch.
(See AdaGram's documentation for a complete list.)

\subsection{Lexicon-embedding mapping}

\begin{table}%[htbp]
  %\footnotesize
  \begin{center}
  \begin{tabular}{ll}
    \textbf{Lexicon} & \textbf{Vector space} \\
    \hline
    \textit{rock-1} `jacket' & \textit{rock-b} (clothing)\\
    \hline
    \textit{rock-2} `rock music' & \textit{rock-a}, \textit{rock-c},\\
    & \textit{rock-d}, \textit{rock-e} (music) \\ 
  \end{tabular}
  \end{center}
  \caption{Example mapping for \textit{rock}.}
  \label{tab:ex}
\end{table}

%Given a lexicon and a trained word sense embedding as described in the preceding 
%sections, 
Our goal is to establish a mapping between lexicographic word senses
and embeddings that represent the same meanings. The approach we take 
is to generate a set of related word senses for each sense of any given word, both
from the lexicon (via relations encoded in its graph's edges) and from the vector
space (via cosine distance), to measure their relatedness and define mappings. 
%Then lexicographic sets of \textit{neighbors} can 
%be compared against sets of \textit{neighboring} embeddings in the vector space to 
%measure their relatedness and define a mapping.

%The methods for obtaining sets of neighbors differ between the lexicon and the
%vector model. 
To obtain sets of neighbors from the lexicon, given any target word sense, any senses directly 
connected by an edge to the target's node is selected as a neighbor. In the vector
space, nearest neighbors based on cosine distance are selected.
%note that this 
%needs a number of neighbors to be specified beforehand.

%Certain difficulties require special attention in the process of comparing sets of
%neighbors. On one hand, 
%geometric distance is arguably an efficient and objective way to measure 
%relatedness between sets of neighbor concepts. 
%To achieve, this, however,
In order to measure relatedness using geometric operations
we need to assign \textit{provisional} embeddings to lexicographic neighbors
in order to measure their distance to vector space neighbors. We do this by
using AdaGram's disambiguation tool: given a target word and a set of context
words, it calculates the posterior probability of each sense of the target 
word given the context words according to the hierarchical softmax model 
(equation~\ref{eq:hs}).
The word in context is disambiguated by selecting the sense with the highest
posterior probability. 
(In our case, for any given sense,
the rest of senses in the set act as context.)
%In our case, we disambiguate the lexicographic
%neighbors one by one by using the rest of neighbors as context words. This 
%assigns one word sense vector to each lexicon word sense present in a 
%set of neighbors.

%An additional source of difficulty is the lack of asymmetry in this mapping:
We expect some lexicon-defined senses to not be present in the vector space 
%(due, for example, to not being well-represented in the training corpus) 
and
some word senses captured by the embedding model to not be listed in the lexicon.
Additionally, the AdaGram model may create two or more senses for one 
word which relate to the same lexicographic sense.
%and thus should be mapped
%to the same concept.
%In fact, managing these missing connections leads to potential applications
%of this mapping strategy: Suggesting missing entries in the lexicon or identifying
%unlisted instances in a corpus.
We address this by making the mapping 1-to-$N$ to allow a lexicon sense
to be linked to more than one sense embedding if necessary. Additionally, in order
to make it possible for lexicon senses to be left unlinked, we
propose to use a \textit{false sense embedding} that acts as an attractor for
those lexicon senses with weak links to real embeddings.
%being linked to
%this false sense embeddings leaves the lexicon sense unlinked in practice.

\begin{algorithm}[]

  %\begin{multicols}{2}
    \begin{algorithmic}%[1]
      \ForAll{words $w$}
	\State{\textit{/* Lexicon neighbors */}}
	\State $n$ $\leftarrow$ \#lexical senses of $w$ 
	\State $s_i$ $\leftarrow$ $i$th lexical sense of $w$, $i \in [1,n]$
	\ForAll{$s_i$}
	  \State $a_i$ $\leftarrow$ set of neighbors of $s_i$
	  \ForAll{neighbor $k$ in $a_i$}
	    \State $v(a_{ik})$ $\leftarrow$ embedding for $a_{ik}$
	  \EndFor
	\EndFor
	\State{\textit{/* Vector space neighbors */}}
	\State $T$ $\leftarrow$ max \#senses per word 
	\State $m$ $\leftarrow$ \#nearest neighbors (NN) per sense 
	\State $z_j$ $\leftarrow$ $j$th sense embedding of $w$, $j \in [1,T]$ 
	\ForAll{$z_j$}
	  \State $b_{jl}$ $\leftarrow$ $l$th NN of $b_j$, $l \in [1,m]$ 
	\EndFor
	\State{\textit{/* Average neighbors */}}
	\ForAll{$i$, $j$}
	  \State $v(a_i)$ $\leftarrow$ avg. vector over $k$ 
	\EndFor
	\ForAll{$j$}
	  \State $v(b_j)$ $\leftarrow$ avg. vector over $l$ 
	\EndFor
	\State{\textit{/* Mapping probabilities */}}
	\ForAll{$i$, $j$}
	  \State $p_{ij}$ $\leftarrow$ softmax($v(a_i) \cdot v(b_j)$) 
	  \State $p_{iN+1}$ $\leftarrow$ softmax($\overrightarrow{0}$)
	\EndFor
	\State{\textit{/* Mapping */}}
	\For{$j \in [1,T]$} 
	  %\State{
	  \If{prior($s_j$) $>$ $\rho$} %\State{
	  \State $r$ $\leftarrow$ $\text{indmax}_i(p_{ij})$ %\\
	    \If{$r \neq N+1$} 
	      \State map $s_r$ to $z_j$ %\\
	    \EndIf
	  \EndIf
	\EndFor
      \EndFor
    \end{algorithmic}
  %\end{multicols}
  \caption{Mapping algorithm.}
  \label{alg:mapping}
\end{algorithm}%


The mapping mechanism is shown in algorithm~\ref{alg:mapping}. For each word in
the vocabulary, 
%its lists of lexicon and vector model senses are retrieved. For
%each of those senses, 
a set of neighbors is generated for each of its senses in the lexicon
and in the vector space. The vector
representations of these neighbors are averaged,
since averaging embeddings has been proven an efficient approach to
representing multi-word semantic units \citep{mikolov2013distributed,kenter2016siamese}.
%This yields $n$ vectors on the lexicon side and
%$T$ vectors on the vector space side. 
%($m$ depends on the a priori
%probability of each sense in the vector model). 
A probability matrix 
$p \in [0,1]^{n \times m}$ is calculated by applying the softmax function to 
pairs of vectors. An extra column is added with scores generated by the softmax on 
zero-valued vectors to account for the false sense. A row in $p$ 
represents the probability that each sense in the vector model corresponds to 
that row's lexicon sense, from which the maximum is obtained to establish
a link. (A threshold $\rho$ exists to avoid low-probability links.)
%Mapping lexicon to vector model senses is then just
%a matter of choosing the maximum probability in each row. In order for the
%maximum probability to be used to create a link, it must be higher than a
%threshold $\rho$ and not belong to the false sense.

%{\SetAlgoNoLine
%\begin{algorithm}
  %\ForEach{word $w$}{
    %\tcc{Lexicon neighbors}
    %$n$ $\leftarrow$ \#lexical senses of $w$ \\
    %$s_i$ $\leftarrow$ $i$th lexical sense of $w$, $i \in [1,n]$ \\ 
    %\ForEach{$s_i$}{
      %$a_i$ $\leftarrow$ set of neighbors of $s_i$ \\ 
      %\ForEach{neighbor $k$ in $a_i$}{
	%$v(a_{ik})$ $\leftarrow$ embedding for $a_{ik}$ \\
      %}
    %}
    %\tcc{Vector space neighbors}
    %$T$ $\leftarrow$ max \#senses per word \\ 
    %$m$ $\leftarrow$ \#nearest neighbors (NN) per sense \\ 
    %$z_j$ $\leftarrow$ $j$th sense embedding of $w$, $j \in [1,T]$ \\
    %\ForEach{$z_j$}{
      %$b_{jl}$ $\leftarrow$ $l$th NN of $b_j$, $l \in [1,m]$ \\
    %}
    %\tcc{Average neighbors}
    %\ForEach{$i$, $j$}{
      %$v(a_i)$ $\leftarrow$ avg. vector over $k$ \\
    %}
    %\ForEach{$j$}{
      %$v(b_j)$ $\leftarrow$ avg. vector over $l$ \\
    %}
    %\tcc{Mapping probabilities}
    %\ForEach{$i$, $j$}{
      %$p_{ij}$ $\leftarrow$ softmax($v(a_i) \cdot v(b_j)$) \\
      %$p_{iN+1}$ $\leftarrow$ softmax($\overrightarrow{0}$) \\
    %}
    %\tcc{Mapping}
    %\For{$j \in [1,T]$}{
      %\If{prior($s_j$) $>$ $\rho$}{
	%$r$ $\leftarrow$ indmax_i ($p_{ij}$) \\
	%\If{$r \neq N+1$}{
	  %map $s_r$ to $z_j$ \\
	%}
      %}
    %}
  %}
  %\vspace{0.3cm}
  %\caption{Mapping algorithm.}
  %\label{alg:mapping}
%\end{algorithm}}

%{\SetAlgoNoLine

An example of the linking performed by this mechanism is shown on table~\ref{tab:ex}.
According to the lexicon SALDO, the noun \textit{rock} has two senses in Swedish:
(1) `jacket' and (2) `rock music'. The word sense embedding model finds five
different meanings for \textit{rock}; upon inspection of their nearest neighbors, which
give an indication of the closest senses to any particular meaning, four of them
relate to the music sense (linked to \textit{rock-2}) 
and one to items of clothing (linked to \textit{rock-1}). 
As can
be seen in table~\ref{tab:ex}, the clothing-related meaning is linked to the sense
meaning `jacket', while the four music-related ones is linked to the sense meaning
`rock music'.

