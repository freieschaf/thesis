import numpy as np
import matplotlib.pyplot as plt

def scatter_vonmisses(c, s, n):
    return np.random.vonmises(c, s, n)

# N = 5000

# x = np.random.rand(N)
# y = np.random.rand(N)

plt.axis('off')

x = scatter_vonmisses(0.5, 1, 100)
y = scatter_vonmisses(0, 100, 100)
plt.scatter(x, y, alpha=0.5, s=10, c='gray', linewidths=0)
# plt.show()

# x = scatter_vonmisses(1, 3, 600)
# y = scatter_vonmisses(-1, 3, 600)
# plt.scatter(x, y, alpha=0.5, s=10, c='gray', linewidths=0)
# plt.show()

plt.savefig('vonmisses_extra.pdf', dpi=600, quality=95, format='pdf', transparent=True)
